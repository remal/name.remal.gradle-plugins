package name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin

import name.remal.gradle_plugins.dsl.PluginCondition
import name.remal.gradle_plugins.dsl.ProjectPluginClass
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.isRootProject
import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import name.remal.uncheckedCast
import org.gradle.api.Project

interface ApplyOnRootProjectMixin {

    @PluginCondition
    fun `Is applied on root project`(project: Project): Boolean {
        if (project.isRootProject) {
            return true

        } else {
            val pluginClass = this.javaClass.unwrapGradleGenerated().uncheckedCast<ProjectPluginClass>()
            project.rootProject.applyPlugin(pluginClass)
            return false
        }
    }

}
