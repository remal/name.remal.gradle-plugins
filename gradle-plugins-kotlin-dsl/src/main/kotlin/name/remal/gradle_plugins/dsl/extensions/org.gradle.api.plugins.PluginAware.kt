package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.PluginId
import org.gradle.api.Plugin
import org.gradle.api.plugins.AppliedPlugin
import org.gradle.api.plugins.PluginAware

fun <T : PluginAware> T.applyPlugin(pluginId: String) = pluginManager.apply(pluginId)
fun <T : PluginAware> T.applyPlugin(type: Class<out Plugin<out T>>) = pluginManager.apply(type)
fun <T : PluginAware> T.withPlugin(pluginId: String, action: (appliedPlugin: AppliedPlugin) -> Unit) = pluginManager.withPlugin(pluginId, action)
fun <T : PluginAware> T.withOneOfPlugin(pluginIds: Collection<String>, action: (appliedPlugin: AppliedPlugin) -> Unit) = pluginManager.withOneOfPlugin(pluginIds, action)
fun <T : PluginAware> T.withOneOfPlugin(vararg pluginIds: String, action: (appliedPlugin: AppliedPlugin) -> Unit) = pluginManager.withOneOfPlugin(ids = *pluginIds, action = action)
fun <T : PluginAware> T.withOneOfPlugin(vararg pluginIds: PluginId, action: (appliedPlugin: AppliedPlugin) -> Unit) = pluginManager.withOneOfPlugin(ids = *pluginIds, action = action)
fun <T : PluginAware> T.applyFirstAvailablePlugin(pluginIds: Collection<String>) = pluginManager.applyFirstAvailable(pluginIds)
fun <T : PluginAware> T.applyFirstAvailablePlugin(vararg pluginIds: String) = pluginManager.applyFirstAvailable(*pluginIds)
