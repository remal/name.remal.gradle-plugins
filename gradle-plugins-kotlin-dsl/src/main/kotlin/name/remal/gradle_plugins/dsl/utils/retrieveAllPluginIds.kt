package name.remal.gradle_plugins.dsl.utils

import io.github.classgraph.ClassGraph
import name.remal.concurrentMapOf
import name.remal.concurrentSetOf
import java.lang.ClassLoader.getSystemClassLoader
import java.util.concurrent.ConcurrentMap

private val pluginIdsCache: ConcurrentMap<ClassLoader, Set<String>> = concurrentMapOf()

private object NullClassLoader : ClassLoader()

fun retrieveAllPluginIds(classLoader: ClassLoader?): Set<String> {
    return pluginIdsCache.computeIfAbsent(classLoader ?: NullClassLoader) { classLoaderKey ->
        val pluginIds = concurrentSetOf<String>()

        ClassGraph()
            .apply {
                if (classLoader === NullClassLoader) {
                    overrideClassLoaders(getSystemClassLoader())
                } else {
                    overrideClassLoaders(classLoaderKey)
                }
            }
            .acceptPathsNonRecursive("META-INF/gradle-plugins")
            .scan()
            .use {
                it.allResources.asSequence()
                    .filter { it.path.endsWith(".properties") }
                    .forEach { resource ->
                        pluginIds.add(resource.path.substringAfterLast('/').substringBeforeLast('.'))
                    }
            }

        return@computeIfAbsent pluginIds.toSortedSet().toSet()
    }
}
