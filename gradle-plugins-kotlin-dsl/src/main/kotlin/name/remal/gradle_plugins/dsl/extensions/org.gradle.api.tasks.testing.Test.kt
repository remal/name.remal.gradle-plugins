package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.tasks.testing.Test
import java.lang.reflect.Field

private val testFrameworkField: Field by lazy {
    Test::class.java.getDeclaredField("testFramework")
        .apply { isAccessible = true };
}

val Test.isTestFrameworkSet: Boolean get() = testFrameworkField.get(this) != null
