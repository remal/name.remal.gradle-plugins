package name.remal.gradle_plugins.dsl.utils

import name.remal.ASM_API
import org.objectweb.asm.AnnotationVisitor
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassReader.SKIP_CODE
import org.objectweb.asm.ClassReader.SKIP_DEBUG
import org.objectweb.asm.ClassReader.SKIP_FRAMES
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.Type

fun isAnnotatedBy(bytecode: ByteArray, annotationDesc: String): Boolean {
    var result = false
    val visitor = object : ClassVisitor(ASM_API) {
        override fun visitAnnotation(desc: String?, visible: Boolean): AnnotationVisitor? {
            if (annotationDesc == desc) result = true
            return null
        }
    }
    ClassReader(bytecode).accept(visitor, SKIP_DEBUG or SKIP_FRAMES or SKIP_CODE)
    return result
}

fun isAnnotatedBy(bytecode: ByteArray, annotationClass: Class<*>) = isAnnotatedBy(bytecode, Type.getDescriptor(annotationClass))
