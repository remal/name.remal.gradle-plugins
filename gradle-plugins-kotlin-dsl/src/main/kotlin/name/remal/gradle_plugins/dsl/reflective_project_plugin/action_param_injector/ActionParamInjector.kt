package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import com.google.common.reflect.TypeToken
import name.remal.asClass
import name.remal.hierarchy
import name.remal.uncheckedCast
import org.gradle.api.Project
import java.lang.reflect.ParameterizedType

abstract class ActionParamInjector<ParamType> : Comparable<ActionParamInjector<*>> {

    abstract fun createValue(project: Project): ParamType

    open val paramType: Class<ParamType> by lazy {
        val type = TypeToken.of(this.javaClass).getSupertype(ActionParamInjector::class.java).type
        if (type !is ParameterizedType) throw IllegalStateException("$type is not instance of ParameterizedType")
        return@lazy type.actualTypeArguments[0].asClass().uncheckedCast<Class<ParamType>>()
    }

    private val hierarchySize: Int by lazy { paramType.hierarchy.size }

    override fun compareTo(other: ActionParamInjector<*>): Int {
        hierarchySize.compareTo(other.hierarchySize).let { if (it != 0) return it }
        paramType.name.compareTo(other.paramType.name).let { if (it != 0) return it }
        return 0
    }

}
