package name.remal.gradle_plugins.dsl.utils

import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

fun getGradleLogger(name: String): Logger = Logging.getLogger(name)
fun getGradleLogger(clazz: Class<*>): Logger = Logging.getLogger(clazz.unwrapGradleGenerated())


private const val COMPANION_CLASS_NAME_SUFFIX = "\$Companion"

fun getGradleLoggerForCurrentClass(): Logger {
    val stackTraceElements: Array<StackTraceElement> = Throwable().stackTrace
    val stackTraceElement = stackTraceElements[1]
    var className = stackTraceElement.className

    val fileName = stackTraceElement.fileName
    if (fileName != null && fileName.endsWith(".kt")) {
        if (className.endsWith(COMPANION_CLASS_NAME_SUFFIX)) {
            className = className.substring(0, className.length - COMPANION_CLASS_NAME_SUFFIX.length)
        }
    }

    return getGradleLogger(className)
}
