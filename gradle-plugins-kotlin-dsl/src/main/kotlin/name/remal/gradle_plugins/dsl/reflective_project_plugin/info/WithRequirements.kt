package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

import name.remal.gradle_plugins.dsl.PluginId
import org.gradle.util.GradleVersion

interface WithRequirements {

    val minGradleVersion: GradleVersion?
    val maxGradleVersion: GradleVersion?
    val supportsCurrentGradleVersion: Boolean
        get() = minGradleVersion.let { it == null || it <= GradleVersion.current() }
            && maxGradleVersion.let { it == null || it >= GradleVersion.current() }
    val doesNotSupportCurrentGradleVersion: Boolean get() = !supportsCurrentGradleVersion

    val requirePluginIds: Set<PluginId>
    val requireOptionalPluginIds: Set<PluginId>

}
