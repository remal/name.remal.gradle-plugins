package name.remal.gradle_plugins.dsl.extensions

import name.remal.isPublic
import name.remal.isStatic
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

@Suppress("DEPRECATION")
fun Method.makeAccessible() = apply {
    if (isAccessible) return@apply
    if (!isPublic || !declaringClass.isPublic) {
        isAccessible = true
    }
}

fun <T : Any?> Method.invokeForInstance(instance: Any, vararg args: Any?): T {
    if (isStatic) throw IllegalStateException("Should be invoked statically: $this")
    makeAccessible()
    try {
        @Suppress("UNCHECKED_CAST")
        return invoke(instance, *args) as T
    } catch (e: InvocationTargetException) {
        throw e.cause ?: e
    }
}

fun <T : Any?> Method.invokeStatically(vararg args: Any?): T {
    if (!isStatic) throw IllegalStateException("Should NOT be invoked statically: $this")
    makeAccessible()
    try {
        @Suppress("UNCHECKED_CAST")
        return invoke(null, *args) as T
    } catch (e: InvocationTargetException) {
        throw e.cause ?: e
    }
}
