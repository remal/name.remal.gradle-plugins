package name.remal.gradle_plugins.dsl.plugins

import name.remal.KotlinAllOpen
import name.remal.gradle_plugins.dsl.extensions.atTheEndOfAfterEvaluationOrNow
import org.gradle.api.Plugin
import org.gradle.api.Project

@KotlinAllOpen
class DisplayProjectVersionPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.atTheEndOfAfterEvaluationOrNow(Int.MAX_VALUE) {
            it.logger.warn("{}: {}", it, it.version)
        }
    }

}
