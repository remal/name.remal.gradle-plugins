package name.remal.gradle_plugins.dsl.extensions

import name.remal.lambda.VoidFunction1
import org.gradle.api.Action

fun <T> VoidFunction1<T>.toConfigureAction() = Action<T> { invoke(it) }
