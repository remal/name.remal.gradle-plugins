package name.remal.gradle_plugins.dsl.extensions

import name.remal.getCompatibleMethod
import name.remal.newTempFile
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.testing.jacoco.tasks.JacocoMerge
import java.io.File
import java.lang.reflect.Method

private val jacocoMergeGetExecutionDataMethod: Method by lazy {
    JacocoMerge::class.java.getCompatibleMethod(FileCollection::class.java, "getExecutionData")
}
private val jacocoMergeSetExecutionDataMethod: Method by lazy {
    JacocoMerge::class.java.getCompatibleMethod("setExecutionData", FileCollection::class.java)
}
var JacocoMerge.executionDataCompatible: FileCollection
    get() = (jacocoMergeGetExecutionDataMethod.invoke(this) as? FileCollection) ?: project.files()
    set(value) {
        val executionData: FileCollection? = jacocoMergeGetExecutionDataMethod.invokeForInstance(this)
        if (executionData == value) return
        if (executionData is ConfigurableFileCollection) {
            executionData.setFrom(value as Any)
        } else {
            jacocoMergeSetExecutionDataMethod.invokeForInstance<Any?>(this, value)
        }
    }


fun JacocoMerge.prepareExecutionData() {
    var result = executionDataCompatible
    result = project.files(*result.files.filter(File::exists).toTypedArray())
    if (result.isEmpty) result = project.files(newTempFile("jacoco-", ".exec"))
    executionDataCompatible = result
}
