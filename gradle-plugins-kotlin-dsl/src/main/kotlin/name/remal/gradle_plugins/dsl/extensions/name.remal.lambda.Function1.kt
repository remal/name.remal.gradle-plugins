package name.remal.gradle_plugins.dsl.extensions

import name.remal.lambda.Function1
import org.gradle.api.Action

fun <T> Function1<*, T>.toConfigureAction() = Action<T> { invoke(it) }
