package name.remal.gradle_plugins.dsl.utils

import name.remal.OS

val executableFileSuffix: String = if (OS.isWindows()) {
    ".exe"
} else {
    ""
}
