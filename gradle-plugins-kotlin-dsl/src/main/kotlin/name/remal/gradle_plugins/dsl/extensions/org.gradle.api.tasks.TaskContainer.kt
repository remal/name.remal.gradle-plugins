package name.remal.gradle_plugins.dsl.extensions

import name.remal.findMethod
import org.gradle.api.Action
import org.gradle.api.Task
import org.gradle.api.tasks.TaskContainer
import java.lang.reflect.Method

private val registerMethodNames = listOf("register", "createLater", "create")
private fun findRegisterMethod(vararg parameterTypes: Class<*>): Method {
    val declaringClass = TaskContainer::class.java
    registerMethodNames.forEach { methodName ->
        return declaringClass.findMethod(methodName, *parameterTypes) ?: return@forEach
    }
    throw IllegalStateException(
        "${declaringClass.name} doesn't have a public method of name ${registerMethodNames.joinToString("/")}" +
            " and parameter types ${parameterTypes.joinToString(", ", transform = { it.name })}"
    )
}

private val registerStringMethod: Method by lazy { findRegisterMethod(String::class.java) }
fun TaskContainer.registerCompatible(name: String) {
    registerStringMethod.invokeForInstance<Any?>(this, name)
}

private val registerStringClassMethod: Method by lazy { findRegisterMethod(String::class.java, Class::class.java) }
fun <T : Task> TaskContainer.registerCompatible(name: String, type: Class<T>) {
    registerStringClassMethod.invokeForInstance<Any?>(this, name, type)
}

private val registerStringActionMethod: Method by lazy { findRegisterMethod(String::class.java, Action::class.java) }
fun TaskContainer.registerCompatible(name: String, configurer: (task: Task) -> Unit) {
    registerStringActionMethod.invokeForInstance<Any?>(this, name, Action<Task> { configurer(it) })
}

private val registerStringClassActionMethod: Method by lazy { findRegisterMethod(String::class.java, Class::class.java, Action::class.java) }
fun <T : Task> TaskContainer.registerCompatible(name: String, type: Class<T>, configurer: (task: T) -> Unit) {
    registerStringClassActionMethod.invokeForInstance<Any?>(this, name, type, Action<T> { configurer(it) })
}
