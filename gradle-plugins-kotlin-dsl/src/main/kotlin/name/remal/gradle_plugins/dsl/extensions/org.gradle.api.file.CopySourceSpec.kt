package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.CopySourceSpec
import java.util.concurrent.Callable

fun CopySourceSpec.from(callable: () -> Any) {
    from(Callable<Any> { callable() })
}
