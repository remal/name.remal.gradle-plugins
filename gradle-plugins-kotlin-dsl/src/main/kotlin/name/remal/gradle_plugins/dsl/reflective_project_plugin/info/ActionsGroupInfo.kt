package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

import name.remal.gradle_plugins.dsl.PluginId
import name.remal.gradle_plugins.dsl.ProjectPluginClass
import org.gradle.util.GradleVersion

data class ActionsGroupInfo(
    val actionsGroupClass: Class<*>,
    val isHidden: Boolean = false,
    override val order: Int = 0,
    override val description: String = "",
    override val conditionMethods: List<ConditionMethodInfo> = listOf(),
    override val minGradleVersion: GradleVersion? = null,
    override val maxGradleVersion: GradleVersion? = null,
    override val requirePluginIds: Set<PluginId> = setOf(),
    override val requireOptionalPluginIds: Set<PluginId> = setOf(),
    override val applyPluginIds: Set<PluginId> = setOf(),
    override val applyOptionalPluginIds: Set<PluginId> = setOf(),
    override val applyPluginClasses: Set<ProjectPluginClass> = setOf(),
    override val applyPluginIdsAtTheEnd: Set<PluginId> = setOf(),
    override val applyOptionalPluginIdsAtTheEnd: Set<PluginId> = setOf(),
    override val applyPluginClassesAtTheEnd: Set<ProjectPluginClass> = setOf(),
    override val actionMethods: List<ActionMethodInfo> = listOf(),
    override val actionsGroups: List<ActionsGroupInfo> = listOf(),
    override val isAfterProjectEvaluation: Boolean = false
) : ActionInfo, WithPluginActions, Comparable<ActionsGroupInfo> {

    override val actions: List<ActionInfo> = super.actions

    override fun compareTo(other: ActionsGroupInfo): Int {
        super.compareTo(other).let { if (it != 0) return it }
        actionsGroupClass.simpleName.compareTo(other.actionsGroupClass.simpleName).let { if (it != 0) return it }
        actionsGroupClass.name.compareTo(other.actionsGroupClass.name).let { if (it != 0) return it }
        return 0
    }

}
