package name.remal.gradle_plugins.dsl.utils

import groovy.lang.GroovySystem
import name.remal.version.Version
import org.gradle.api.GradleException

fun getGradleGroovyVersion(): Version {
    val version: String = try {
        GroovySystem.getVersion()

    } catch (e: NoSuchMethodError) {
        // for Groovy <1.6, we need to call org.codehaus.groovy.runtime.InvokerHelper#getVersion
        try {
            val ih = Class.forName("org.codehaus.groovy.runtime.InvokerHelper")
            val getVersion = ih.getDeclaredMethod("getVersion")
            getVersion.invoke(ih) as String

        } catch (e: Exception) {
            throw GradleException("Unable to determine Groovy version", e)
        }
    }

    return Version.parse(version)
}
