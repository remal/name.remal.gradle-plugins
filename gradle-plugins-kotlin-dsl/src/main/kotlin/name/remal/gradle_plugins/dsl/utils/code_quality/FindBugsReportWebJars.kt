package name.remal.gradle_plugins.dsl.utils.code_quality

import io.github.classgraph.ClassGraph
import name.remal.packageName
import java.util.TreeMap

internal object FindBugsReportWebJars {

    private val orders = mapOf(
        Regex("\\bjquery\\b") to 0,
        Regex("\\bpopper(js)?\\b") to 1,
        Regex("\\bbootstrap\\b") to 2,
        Regex("\\bprism\\b") to 3,
        Regex(".") to Int.MAX_VALUE
    ).toList()

    internal val resources: Map<String, ByteArray> by lazy {
        val result = TreeMap<String, ByteArray>(Comparator { path1, path2 ->
            val order1 = orders.first { it.first.containsMatchIn(path1) }.second
            val order2 = orders.first { it.first.containsMatchIn(path2) }.second
            order1.compareTo(order2).let { if (it != 0) return@Comparator it }

            val dir1 = path1.substringBeforeLast('/', "")
            val dir2 = path2.substringBeforeLast('/', "")
            dir1.compareTo(dir2).let { if (it != 0) return@Comparator it }

            val name1 = path1.substringAfterLast('/')
            val name2 = path2.substringAfterLast('/')
            name1.substringBefore('.', "").compareTo(name2.substringBefore('.', "")).let { if (it != 0) return@Comparator it }
            name1.compareTo(name2).let { if (it != 0) return@Comparator it }

            return@Comparator path1.compareTo(path2)
        })

        val basePath = FindBugsReportWebJars.javaClass.packageName.replace('.', '/') + "/assets"
        ClassGraph()
            .overrideClassLoaders(FindBugsReportWebJars.javaClass.classLoader)
            .acceptPaths(basePath)
            .scan()
            .use {
                it.allResources.asSequence()
                    .filter { it.path.startsWith(basePath) }
                    .forEach { resource ->
                        val relativePath = resource.path.substring(basePath.length).trim('/')
                        if (relativePath.isNotEmpty()) {
                            result[relativePath] = resource.load()
                        }
                    }
            }
        return@lazy result.toMap()
    }

}
