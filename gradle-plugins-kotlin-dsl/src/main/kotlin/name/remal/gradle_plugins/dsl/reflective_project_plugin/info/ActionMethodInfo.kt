package name.remal.gradle_plugins.dsl.reflective_project_plugin.info

import name.remal.gradle_plugins.dsl.PluginId
import name.remal.gradle_plugins.dsl.ProjectPluginClass
import org.gradle.util.GradleVersion
import java.lang.reflect.Method
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.javaMethod

data class ActionMethodInfo(
    val method: Method,
    val isHidden: Boolean = false,
    override val order: Int = 0,
    override val description: String = "",
    override val minGradleVersion: GradleVersion? = null,
    override val maxGradleVersion: GradleVersion? = null,
    override val requirePluginIds: Set<PluginId> = setOf(),
    override val requireOptionalPluginIds: Set<PluginId> = setOf(),
    override val applyPluginIds: Set<PluginId> = setOf(),
    override val applyOptionalPluginIds: Set<PluginId> = setOf(),
    override val applyPluginClasses: Set<ProjectPluginClass> = setOf(),
    override val applyPluginIdsAtTheEnd: Set<PluginId> = setOf(),
    override val applyOptionalPluginIdsAtTheEnd: Set<PluginId> = setOf(),
    override val applyPluginClassesAtTheEnd: Set<ProjectPluginClass> = setOf(),
    val createExtensionInfo: CreateExtensionInfo? = null,
    override val isAfterProjectEvaluation: Boolean = false
) : ActionInfo, Comparable<ActionMethodInfo> {

    constructor(
        kFunction: KFunction<*>,
        isHidden: Boolean = false,
        order: Int = 0,
        description: String = "",
        minGradleVersion: GradleVersion? = null,
        maxGradleVersion: GradleVersion? = null,
        requirePluginIds: Set<PluginId> = setOf(),
        applyPluginIds: Set<PluginId> = setOf(),
        applyOptionalPluginIds: Set<PluginId> = setOf(),
        applyPluginClasses: Set<ProjectPluginClass> = setOf(),
        applyPluginIdsAtTheEnd: Set<PluginId> = setOf(),
        applyOptionalPluginIdsAtTheEnd: Set<PluginId> = setOf(),
        applyPluginClassesAtTheEnd: Set<ProjectPluginClass> = setOf(),
        createExtensionInfo: CreateExtensionInfo? = null,
        isAfterProjectEvaluation: Boolean = false
    ) : this(
        method = kFunction.javaMethod ?: throw IllegalArgumentException("$kFunction can't be represented by Java method"),
        isHidden = isHidden,
        order = order,
        description = description,
        minGradleVersion = minGradleVersion,
        maxGradleVersion = maxGradleVersion,
        requirePluginIds = requirePluginIds,
        applyPluginIds = applyPluginIds,
        applyOptionalPluginIds = applyOptionalPluginIds,
        applyPluginClasses = applyPluginClasses,
        applyPluginIdsAtTheEnd = applyPluginIdsAtTheEnd,
        applyOptionalPluginIdsAtTheEnd = applyOptionalPluginIdsAtTheEnd,
        applyPluginClassesAtTheEnd = applyPluginClassesAtTheEnd,
        createExtensionInfo = createExtensionInfo,
        isAfterProjectEvaluation = isAfterProjectEvaluation
    )

    override fun compareTo(other: ActionMethodInfo): Int {
        super.compareTo(other).let { if (it != 0) return it }
        method.declaringClass.name.compareTo(other.method.declaringClass.name).let { if (it != 0) return it }
        method.name.compareTo(other.method.name).let { if (it != 0) return it }
        method.toString().compareTo(other.method.toString()).let { if (it != 0) return it }
        return 0
    }

}
