package name.remal.gradle_plugins.dsl.utils

import name.remal.ASM_API
import org.objectweb.asm.AnnotationVisitor
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.FieldVisitor
import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.TypePath

class SkipInvisibleAnnotationsClassVisitor(api: Int, delegate: ClassVisitor?) : ClassVisitor(api, delegate) {

    constructor(delegate: ClassVisitor?) : this(ASM_API, delegate)

    override fun visitAnnotation(descriptor: String, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitAnnotation(descriptor, visible)
    }

    override fun visitTypeAnnotation(typeRef: Int, typePath: TypePath?, descriptor: String?, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitTypeAnnotation(typeRef, typePath, descriptor, visible)
    }

    override fun visitField(access: Int, name: String, descriptor: String, signature: String?, value: Any?): FieldVisitor? {
        return super.visitField(access, name, descriptor, signature, value)?.let {
            SkipInvisibleAnnotationsFieldVisitor(api, it)
        }
    }

    override fun visitMethod(access: Int, name: String, descriptor: String, signature: String?, exceptions: Array<String>?): MethodVisitor? {
        return super.visitMethod(access, name, descriptor, signature, exceptions)?.let {
            SkipInvisibleAnnotationsMethodVisitor(api, it)
        }
    }

}

class SkipInvisibleAnnotationsFieldVisitor(api: Int, delegate: FieldVisitor?) : FieldVisitor(api, delegate) {

    constructor(delegate: FieldVisitor?) : this(ASM_API, delegate)

    override fun visitAnnotation(descriptor: String, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitAnnotation(descriptor, visible)
    }

    override fun visitTypeAnnotation(typeRef: Int, typePath: TypePath?, descriptor: String?, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitTypeAnnotation(typeRef, typePath, descriptor, visible)
    }

}

class SkipInvisibleAnnotationsMethodVisitor(api: Int, delegate: MethodVisitor?) : MethodVisitor(api, delegate) {

    constructor(delegate: MethodVisitor?) : this(ASM_API, delegate)

    override fun visitAnnotation(descriptor: String, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitAnnotation(descriptor, visible)
    }

    override fun visitTypeAnnotation(typeRef: Int, typePath: TypePath?, descriptor: String?, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitTypeAnnotation(typeRef, typePath, descriptor, visible)
    }

    override fun visitParameterAnnotation(parameter: Int, descriptor: String, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitParameterAnnotation(parameter, descriptor, visible)
    }

    override fun visitLocalVariableAnnotation(
        typeRef: Int,
        typePath: TypePath?,
        start: Array<out Label>?,
        end: Array<out Label>?,
        index: IntArray?,
        descriptor: String?,
        visible: Boolean
    ): AnnotationVisitor? {
        if (!visible) return null
        return super.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible)
    }

    override fun visitInsnAnnotation(typeRef: Int, typePath: TypePath?, descriptor: String?, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitInsnAnnotation(typeRef, typePath, descriptor, visible)
    }

    override fun visitTryCatchAnnotation(typeRef: Int, typePath: TypePath?, descriptor: String?, visible: Boolean): AnnotationVisitor? {
        if (!visible) return null
        return super.visitTryCatchAnnotation(typeRef, typePath, descriptor, visible)
    }

}
