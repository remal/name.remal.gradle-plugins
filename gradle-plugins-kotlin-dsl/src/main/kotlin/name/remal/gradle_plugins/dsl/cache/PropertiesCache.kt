package name.remal.gradle_plugins.dsl.cache

import name.remal.buildMap
import name.remal.loadProperties
import name.remal.storeAsString
import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets.UTF_8
import java.util.Properties

class PropertiesCache(cacheId: String, version: Int) : BaseCache<Map<String, String>, Map<String, Any?>>("$cacheId.properties", version) {

    override val serializer: (cacheValue: Map<String, Any?>) -> ByteArray? = serializer@{ cacheValue ->
        val props = Properties()
        cacheValue.forEach { key, value ->
            if (value != null) {
                props[key] = value.toString()
            }
        }
        if (props.isEmpty) {
            return@serializer null
        } else {
            return@serializer props.storeAsString().toByteArray(UTF_8)
        }
    }

    override val deserializer: (bytes: ByteArray) -> Map<String, String>? = deserializer@{ bytes ->
        if (bytes.isEmpty()) {
            return@deserializer null

        } else {
            return@deserializer buildMap {
                val props = loadProperties(ByteArrayInputStream(bytes))
                props.forEach { key, value ->
                    if (key != null && value != null) {
                        put(key.toString(), value.toString())
                    }
                }
            }
        }
    }

}
