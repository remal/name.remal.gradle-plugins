package name.remal.gradle_plugins.dsl.utils.code_quality

import org.gradle.api.Task
import java.io.File
import java.io.OutputStream
import java.io.Writer

fun FindBugsReport.writeHtmlTo(outputStream: OutputStream, task: Task) = writeHtmlTo(outputStream, task.project.projectDir, task.findBugsReportToolName, task.project.rootDir)
fun FindBugsReport.writeHtmlTo(writer: Writer, task: Task) = writeHtmlTo(writer, task.project.projectDir, task.findBugsReportToolName, task.project.rootDir)
fun FindBugsReport.writeHtmlTo(file: File, task: Task) = writeHtmlTo(file, task.project.projectDir, task.findBugsReportToolName, task.project.rootDir)
fun FindBugsReport.asHtmlString(task: Task) = asHtmlString(task.project.projectDir, task.findBugsReportToolName, task.project.rootDir)
