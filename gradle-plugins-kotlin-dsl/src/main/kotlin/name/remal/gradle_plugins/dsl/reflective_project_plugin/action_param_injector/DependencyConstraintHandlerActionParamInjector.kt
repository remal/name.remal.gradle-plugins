package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyConstraintHandler

class DependencyConstraintHandlerActionParamInjector : ActionParamInjector<DependencyConstraintHandler>() {
    override fun createValue(project: Project): DependencyConstraintHandler = project.dependencies.constraints
}
