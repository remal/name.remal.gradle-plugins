package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.PolymorphicDomainObjectContainer

fun <T> PolymorphicDomainObjectContainer<T>.getOrCreate(name: String, configurer: (T) -> Unit = {}): T {
    return findByName(name) ?: create(name, configurer)
}

fun <T, S : T> PolymorphicDomainObjectContainer<T>.getOrCreate(name: String, type: Class<S>, configurer: (S) -> Unit = {}): T {
    return findByName(name) ?: create(name, type, configurer)
}

fun <T, S : T> PolymorphicDomainObjectContainer<T>.createWithUniqueName(namePrefix: String, type: Class<S>, configurer: (S) -> Unit = {}): S {
    removeDefaultObjects()

    var num = 0
    while (true) {
        ++num
        val nameWithSuffix = "$namePrefix\$$num"
        if (nameWithSuffix !in this) {
            return create(nameWithSuffix, type, configurer)
        }
    }
}

fun <T, S : T> PolymorphicDomainObjectContainer<T>.createWithUniqueName(type: Class<S>, configurer: (S) -> Unit = {}): S = createWithUniqueName(type.simpleName, type, configurer)

fun <T, S : T> PolymorphicDomainObjectContainer<T>.createWithOptionalUniqueSuffix(name: String, type: Class<S>, configurer: (S) -> Unit = {}): S {
    removeDefaultObjects()

    if (name !in this) {
        return create(name, type, configurer)
    }

    var num = 0
    while (true) {
        ++num
        val nameWithSuffix = "$name\$$num"
        if (nameWithSuffix !in this) {
            return create(nameWithSuffix, type, configurer)
        }
    }
}

