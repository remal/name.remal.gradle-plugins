package name.remal.gradle_plugins.dsl

import java.time.Duration
import kotlin.text.RegexOption.IGNORE_CASE

val SNAPSHOT_REGEX = Regex("\\bSNAPSHOT\\b", IGNORE_CASE)


val DEFAULT_IO_TIMEOUT: Duration = Duration.ofMinutes(5)


val IMPLEMENTATION_CLASS_PLUGIN_DESCRIPTOR_KEY = "implementation-class"
val IS_HIDDEN_DESCRIPTOR_KEY = "x-is-hidden"
val DISPLAY_NAME_PLUGIN_DESCRIPTOR_KEY = "x-display-name"
val DESCRIPTION_PLUGIN_DESCRIPTOR_KEY = "x-description"
val WEBSITE_PLUGIN_DESCRIPTOR_KEY = "x-website"
val TAGS_PLUGIN_DESCRIPTOR_KEY = "x-tags"
val MIN_GRADLE_VERSION_PLUGIN_DESCRIPTOR_KEY = "x-min-gradle-version"
val MAX_GRADLE_VERSION_PLUGIN_DESCRIPTOR_KEY = "x-max-gradle-version"
val IS_DEPRECATED_PLUGIN_DESCRIPTOR_KEY = "x-is-deprecated"
val DEPRECATED_REASON_PLUGIN_DESCRIPTOR_KEY = "x-deprecated-reason"

