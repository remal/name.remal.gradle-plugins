package name.remal.gradle_plugins.dsl.extensions

import name.remal.default
import name.remal.gradle_plugins.dsl.utils.canonizeText
import name.remal.version.Version
import org.gradle.internal.jvm.JavaInfo
import java.nio.charset.StandardCharsets.UTF_8

fun JavaInfo.retrieveVersion(): Version? {
    val command = arrayOf(javaExecutable.absolutePath, "-version")
    val process = ProcessBuilder().command(*command).start()
    val exitValue = process.waitFor()
    if (exitValue != 0) {
        throw IllegalStateException("\"${command.joinToString("\", \"")}\" returned $exitValue")
    }

    process.inputStream?.readAll()?.let(::parseJavaVersionOutput)?.let { return it }
    process.errorStream?.readAll()?.let(::parseJavaVersionOutput)?.let { return it }

    return null
}

private fun parseJavaVersionOutput(bytes: ByteArray): Version? {
    val text = canonizeText(bytes.toString(UTF_8)).default()
    val lines = text.split('\n')
    for (line in lines) {
        val versionStr = line.toLowerCase()
            .substringAfter("version", "") // should contain 'version'
            .trim('\t', ' ', '"', '\'') // remove redundant chars
            .substringBefore(' ') // support '"11.0.9" 2020-10-20'
            .trim('\t', ' ', '"', '\'') // remove redundant chars
        if (versionStr.isNotEmpty()) {
            return Version.parse(versionStr)
        }
    }
    return null
}
