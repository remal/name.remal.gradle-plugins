package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Action
import java.util.function.Consumer

fun <T> Consumer<T>.toConfigureAction() = Action<T> { accept(it) }
