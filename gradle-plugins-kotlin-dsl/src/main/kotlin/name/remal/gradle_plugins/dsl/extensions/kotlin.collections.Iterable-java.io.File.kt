package name.remal.gradle_plugins.dsl.extensions

import name.remal.forThreadContextClassLoader
import name.remal.gradle_plugins.dsl.utils.ClassLoaderProvider
import name.remal.reflection.ExtendedURLClassLoader
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.PARENT_FIRST
import name.remal.reflection.ExtendedURLClassLoader.LoadingOrderFactory
import java.io.File
import java.net.URLClassLoader

fun newLoggingLoadingOrderFactory(defaultLoadingOrder: LoadingOrderFactory) = LoadingOrderFactory order@{ resourceName ->
    if (arrayOf(
            "org/slf4j/",
            "org/apache/log4j/",
            "org/apache/logging/log4j/",
            "org/apache/commons/logging/"
        ).any { resourceName.startsWith(it) }
    ) {
        return@order PARENT_FIRST
    }

    return@order defaultLoadingOrder.getLoadingOrder(resourceName)
}

fun newLoggingLoadingOrderFactory(defaultLoadingOrder: (resourceName: String) -> LoadingOrder) = newLoggingLoadingOrderFactory(LoadingOrderFactory { defaultLoadingOrder(it) })

fun newLoggingLoadingOrderFactory(loadingOrder: LoadingOrder) = newLoggingLoadingOrderFactory(LoadingOrderFactory { loadingOrder })


fun <R> Iterable<File>.forClassLoader(
    loadingOrder: (resourceName: String) -> LoadingOrder,
    parent: ClassLoader? = ClassLoaderProvider::class.java.classLoader,
    action: (classLoader: URLClassLoader) -> R
): R {
    val loadingOrderFactory = LoadingOrderFactory order@{ resourceName ->
        if (resourceName.startsWith("org/gradle/")) return@order PARENT_FIRST
        if (resourceName.startsWith("kotlin/")) return@order PARENT_FIRST
        if (resourceName.startsWith("groovy/")) return@order PARENT_FIRST
        return@order loadingOrder(resourceName)
    }
    val loggingOrderFactory = newLoggingLoadingOrderFactory(loadingOrderFactory)
    val urls = map { it.toURI().toURL() }
    ExtendedURLClassLoader(loggingOrderFactory, urls, parent).use { classLoader ->
        forThreadContextClassLoader(classLoader) {
            return action(classLoader)
        }
    }
}

fun <R> Iterable<File>.forClassLoader(loadingOrder: LoadingOrder, parent: ClassLoader? = ClassLoaderProvider::class.java.classLoader, action: (classLoader: URLClassLoader) -> R): R {
    return forClassLoader({ loadingOrder }, parent, action)
}

fun <R> Iterable<File>.forClassLoader(parent: ClassLoader? = ClassLoaderProvider::class.java.classLoader, action: (classLoader: URLClassLoader) -> R): R {
    return forClassLoader(PARENT_FIRST, parent, action)
}
