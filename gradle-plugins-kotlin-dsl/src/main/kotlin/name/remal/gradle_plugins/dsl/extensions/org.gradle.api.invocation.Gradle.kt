package name.remal.gradle_plugins.dsl.extensions

import name.remal.error
import name.remal.gradle_plugins.dsl.utils.getGradleLoggerForCurrentClass
import org.gradle.api.invocation.Gradle

@Volatile
internal var GRADLE: Gradle? = null


fun Gradle.registerCloseable(closeable: AutoCloseable) {
    buildFinished {
        try {
            closeable.close()

        } catch (e: Exception) {
            fileLogger.error(e)
        }
    }
}


private val fileLogger = getGradleLoggerForCurrentClass()
