package name.remal.gradle_plugins.dsl.extensions

import java.net.HttpURLConnection
import java.net.URLConnection

fun URLConnection.readAllBytes(): ByteArray {
    try {
        return getInputStream().readAll()
    } finally {
        if (this is HttpURLConnection) {
            disconnect()
        }
    }
}
