package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.SourceDirectorySet

fun SourceDirectorySet.makeSrcDirsUnique() = apply {
    setSrcDirs(LinkedHashSet(srcDirs))
}
