package name.remal.gradle_plugins.dsl.extensions

import name.remal.escapeRegex
import org.gradle.api.artifacts.ArtifactRepositoryContainer.DEFAULT_MAVEN_CENTRAL_REPO_NAME
import org.gradle.api.artifacts.ArtifactRepositoryContainer.MAVEN_CENTRAL_URL
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.ArtifactRepository
import org.gradle.api.artifacts.repositories.MavenArtifactRepository

private fun String?.hostEndsWith(host: String) = this != null && (this == host || endsWith(".$host"))

val RepositoryHandler.hasRepositoryWithDynamicVersionsSupport: Boolean
    get() = any { repo ->
        if (repo is MavenArtifactRepository) {
            val url = repo.url ?: return@any false
            val scheme = url.scheme
            val host = url.host
            if (scheme == "http" || scheme == "https") {
                if (host.hostEndsWith("bintray.com")
                    || host.hostEndsWith("jfrog.org")
                    || host.hostEndsWith("maven.org")
                    || host.hostEndsWith("maven.apache.org")
                    || host.hostEndsWith("sonatype.org")
                ) {
                    return@any true
                }
            }
        }
        return@any false
    }


fun RepositoryHandler.maven(repositoryName: String, repositoryUrl: Any? = null, configurer: (repository: MavenArtifactRepository) -> Unit = {}) =
    maven {
        it.name = repositoryName
        if (repositoryUrl != null) {
            it.setUrl(repositoryUrl)
        }
        it.apply(configurer)
    }


private fun ArtifactRepository.matches(nameRegex: Regex, url: String): Boolean {
    if (!nameRegex.matches(name)) {
        return false
    }

    if (this is MavenArtifactRepository) {
        if (this.url.toString() != url) {
            return false
        }
    }

    return true
}


private val mavenCentralRepositoryNameRegex = Regex(escapeRegex(DEFAULT_MAVEN_CENTRAL_REPO_NAME) + "\\d*")
private val mavenCentralRepositoryUri = MAVEN_CENTRAL_URL
private val ArtifactRepository.isMavenCentral: Boolean get() = matches(mavenCentralRepositoryNameRegex, mavenCentralRepositoryUri)

fun RepositoryHandler.forMavenCentral(configurer: (repository: MavenArtifactRepository) -> Unit) {
    withType(MavenArtifactRepository::class.java).matching(ArtifactRepository::isMavenCentral).all(configurer)
}

val RepositoryHandler.isMavenCentralAdded: Boolean
    get() = any(ArtifactRepository::isMavenCentral)

fun RepositoryHandler.mavenCentralIfNotAdded() {
    if (isMavenCentralAdded) return
    mavenCentral()
}

fun RepositoryHandler.mavenCentralIfNotAdded(
    repositoryName: String,
    configurer: (repository: MavenArtifactRepository) -> Unit = {}
) {
    if (isMavenCentralAdded) return

    if (mavenCentralRepositoryNameRegex.matches(repositoryName)) {
        throw IllegalArgumentException("Repository name can't match '$mavenCentralRepositoryNameRegex'")
    }

    maven(repositoryName, mavenCentralRepositoryUri, configurer)
}
