package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.tasks.compile.CompileOptions

var CompileOptions.parameters: Boolean
    get() = "-parameters" in compilerArgs
    set(value) {
        val compilerArgs = this.compilerArgs
        if (value) {
            if ("-parameters" !in compilerArgs) {
                compilerArgs.add("-parameters")
            }
        } else {
            compilerArgs.remove("-parameters")
        }
    }
