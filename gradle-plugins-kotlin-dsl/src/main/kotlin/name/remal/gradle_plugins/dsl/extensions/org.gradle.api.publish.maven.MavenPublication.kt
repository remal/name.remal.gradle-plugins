package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.publish.maven.MavenPublication

val MavenPublication.notation
    get() = DependencyNotation(
        group = groupId,
        module = artifactId,
        version = version
    )


fun DependencyNotationMatcher.matches(publication: MavenPublication) = matches(publication.notation)
fun DependencyNotationMatcher.notMatches(publication: MavenPublication) = !matches(publication)
