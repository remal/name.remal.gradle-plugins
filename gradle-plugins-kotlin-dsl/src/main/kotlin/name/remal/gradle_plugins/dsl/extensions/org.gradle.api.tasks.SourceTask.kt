package name.remal.gradle_plugins.dsl.extensions

import name.remal.buildSet
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceTask
import java.io.File

val SourceTask.sourceDirs: Set<File>
    get() = buildSet {
        source.visit { details ->
            val segments = details.relativePath.segments.size
            if (segments <= 2) {
                var file = details.file.absoluteFile
                repeat(segments) {
                    file = file.parentFile ?: return@visit
                }
                add(file)
            }
        }
    }


fun SourceTask.isProcessingSourceSet(sourceSet: SourceSet): Boolean {
    var result = false
    val sourceSetFiles = sourceSet.allSource
    source.visit { details ->
        if (details.file in sourceSetFiles) {
            result = true
            details.stopVisiting()
        }
    }
    return result
}
