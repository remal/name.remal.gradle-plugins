package name.remal.gradle_plugins.dsl

import name.remal.debug
import name.remal.findMethod
import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperty
import name.remal.gradle_plugins.dsl.artifact.ArtifactsCacheCleanerPlugin
import name.remal.gradle_plugins.dsl.extensions.GRADLE
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.dsl.utils.getPluginIdForLogging
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.util.GradleVersion
import org.gradle.wrapper.GradleUserHomeLookup.gradleUserHome
import java.io.File
import java.net.URL
import java.net.URLConnection
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.LazyThreadSafetyMode.NONE
import org.gradle.api.Plugin as GradlePlugin

abstract class BaseProjectPlugin : GradlePlugin<Project> {

    companion object {
        private val isSnapshotVersionLogged = AtomicBoolean(false)
    }

    protected abstract fun applyImpl(project: Project)

    final override fun apply(project: Project) {
        GRADLE = project.gradle

        if (isSnapshotVersionLogged.compareAndSet(false, true)) {
            val version = getStringProperty("version")
            if (version.contains("SNAPSHOT", true)) {
                val message = buildString {
                    append("|| ")
                    append(getStringProperty("group"))
                    append('.')
                    append(getStringProperty("name"))
                    append(": ")
                    append(version)
                    append(" ||")
                }
                logger.warn(buildString {
                    repeat(message.length) { append('=') }
                    append('\n')
                    append(message)
                    append('\n')
                    repeat(message.length) { append('=') }
                })
            }
        }

        project.applyPlugin(ArtifactsCacheCleanerPlugin::class.java)
        applyImpl(project)
    }


    protected val logger = getGradleLogger(this.javaClass.unwrapGradleGenerated())


    protected open val requiredGradleVersion: GradleVersion get() = GradleVersion.version(getStringProperty("gradle.min-version"))

    init {
        if (!JavaVersion.current().isJava8Compatible) throw IllegalStateException("Current Java version is not compatible with Java 8")
        val current = GradleVersion.current()
        val required = requiredGradleVersion
        if (current < required) throw IllegalStateException("Too old Gradle version - ${current.version} (at minimum ${required.version} required)")
    }


    init { // Disable JarURLConnections caching
        try {
            URLConnection::class.java.findMethod("setDefaultUseCaches", String::class.java, Boolean::class.java)
                ?.apply { isAccessible = true }
                ?.invoke(null, "jar", false)

        } catch (e: Exception) {
            logger.debug(e)
            URL("jar:file://dummy.jar!/").openConnection().defaultUseCaches = false
        }
    }


    protected val pluginId: String by lazy(NONE) { getPluginIdForLogging(javaClass) }

    protected val pluginCacheDir: File by lazy { File(gradleUserHome(), "caches/$pluginId") }

}
