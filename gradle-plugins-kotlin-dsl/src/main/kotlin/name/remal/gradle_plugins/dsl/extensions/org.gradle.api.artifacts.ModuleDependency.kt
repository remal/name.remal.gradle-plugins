package name.remal.gradle_plugins.dsl.extensions

import name.remal.buildMap
import org.gradle.api.artifacts.ExcludeRule.GROUP_KEY
import org.gradle.api.artifacts.ExcludeRule.MODULE_KEY
import org.gradle.api.artifacts.ModuleDependency

fun ModuleDependency.exclude(group: String? = null, module: String? = null) = exclude(buildMap {
    if (!group.isNullOrEmpty() && group != "*") put(GROUP_KEY, group)
    if (!module.isNullOrEmpty() && module != "*") put(MODULE_KEY, module)
})

fun ModuleDependency.exclude(notation: String) = notation.split(':').let { parsedNotation ->
    exclude(parsedNotation.getOrNull(0), parsedNotation.getOrNull(1))
}
