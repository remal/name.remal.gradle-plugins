package name.remal.gradle_plugins.dsl.artifact

import name.remal.classNameToResourceName
import name.remal.gradle_plugins.dsl.extensions.readAll
import java.io.InputStream
import java.util.jar.JarFile.MANIFEST_NAME
import java.util.jar.Manifest

interface HasEntries {

    val entryNames: Set<String>

    operator fun contains(entryName: String): Boolean = entryName in entryNames
    fun openStream(entryName: String): InputStream
    fun readBytes(entryName: String): ByteArray = openStream(entryName).readAll()

    val classEntryNames: Set<String>
    val classNames: Set<String>

    fun containsClass(className: String): Boolean = className in classNames
    fun containsClass(clazz: Class<*>): Boolean = containsClass(clazz.name)
    fun openStreamForClass(className: String): InputStream = openStream(classNameToResourceName(className))
    fun openStreamForClass(clazz: Class<*>): InputStream = openStreamForClass(clazz.name)
    fun readBytecode(className: String): ByteArray = openStreamForClass(className).readAll()
    fun readBytecode(clazz: Class<*>): ByteArray = openStreamForClass(clazz).readAll()

    val manifestMainAttributes: Map<String, String>

    fun readManifest(): Manifest? {
        try {
            openStream(MANIFEST_NAME).use {
                return Manifest(it)
            }
        } catch (e: ArtifactFileNotFoundException) {
            return null
        } catch (e: ArtifactEntryNotFoundException) {
            return null
        }
    }

    class Entry(val name: String, inputStreamProvider: () -> InputStream) {
        val inputStream: InputStream by lazy(inputStreamProvider)
    }

    fun forEachEntry(pattern: String? = null, action: (entry: Entry) -> Unit)

}
