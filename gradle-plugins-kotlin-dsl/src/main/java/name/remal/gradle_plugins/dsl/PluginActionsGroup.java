package name.remal.gradle_plugins.dsl;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Inherited
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface PluginActionsGroup {

    String value() default "";

    int order() default 0;

    boolean isHidden() default false;

}
