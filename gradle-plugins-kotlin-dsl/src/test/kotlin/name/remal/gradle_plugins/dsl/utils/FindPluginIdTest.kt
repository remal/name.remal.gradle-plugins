package name.remal.gradle_plugins.dsl.utils

import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.plugins.JavaPlugin
import org.junit.Test

class FindPluginIdTest {

    @Test
    fun javaPlugin() {
        val pluginId = findPluginId(JavaPlugin::class.java)
        assertThat(pluginId)
            .isNotNull()
            .isNotEmpty()
            .isEqualTo("org.gradle.java")
    }

}
