package name.remal.gradle_plugins.dsl.extensions

import name.remal.version.Version
import org.gradle.api.JavaVersion
import org.gradle.internal.jvm.Jvm
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import java.io.File

class JavaInfoExtensionTest {

    @Test
    fun testJavaHome() {
        val javaHome = System.getProperty("java.home")
        assertFalse(javaHome.isNullOrBlank())

        val javaHomeDir = File(javaHome).absoluteFile
        val jvmInfo = Jvm.forHome(javaHomeDir)

        var version = jvmInfo.retrieveVersion()
        if (version == null) throw AssertionError("Version can't be retrieved")

        if (version.major == 1 && version.numbersCount >= 2) {
            // For legacy Java version (for example: 1.8.0_151)
            version = Version.create(
                (1 until version.numbersCount)
                    .map(version::getNumberOr0)
                    .toTypedArray()
                    .toIntArray(),
                version.suffixDelimiter,
                version.suffix
            )
        }
        assertEquals(JavaVersion.current().majorVersion, version.major.toString())
    }

}
