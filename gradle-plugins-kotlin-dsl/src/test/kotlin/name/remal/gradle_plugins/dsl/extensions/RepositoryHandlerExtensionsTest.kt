package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test

class RepositoryHandlerExtensionsTest : BaseProjectTest() {

    @Test
    fun mavenCentralIfNotAdded_added() {
        assertEquals(0, project.repositories.size)
        project.repositories.mavenCentral()
        assertEquals(1, project.repositories.size)
        project.repositories.mavenCentralIfNotAdded()
        assertEquals(1, project.repositories.size)
    }

    @Test
    fun mavenCentralIfNotAdded_notAdded() {
        assertEquals(0, project.repositories.size)
        project.repositories.mavenCentralIfNotAdded()
        assertEquals(1, project.repositories.size)
        project.repositories.mavenCentralIfNotAdded()
        assertEquals(1, project.repositories.size)
    }

}
