**This plugin works only if [`application`](https://docs.gradle.org/current/userguide/application_plugin.html) plugin is applied.**

The plugin automatically sets [`application.mainClassName`](https://docs.gradle.org/current/javadoc/org/gradle/api/plugins/ApplicationPluginConvention.html#setMainClassName-java.lang.String-) by parsing compiled classes.
