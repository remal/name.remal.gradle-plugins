**This plugin works only if [`kotlin2js`](https://kotlinlang.org/docs/reference/using-gradle.html) plugin is applied.**

The plugin applies [`name.remal.java-settings`](name.remal.java-settings.md) plugin.

&nbsp;

This plugin helps to configure [`kotlin`](https://kotlinlang.org/docs/reference/using-gradle.html) plugin.

For all `Kotlin2JsCompile` tasks:

* Sets `metaInfo` to `true`
* Sets `sourceMap` to `true`
* Sets `moduleKind` to `umd`
* Sets `noStdlib` to `false`
