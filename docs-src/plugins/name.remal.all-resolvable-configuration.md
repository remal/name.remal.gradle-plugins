This plugin creates `allResolvable` configuration and makes all resolvable configurations to extend it.

For example, it's useful for declaring platforms for all configurations:

```groovy
dependencies {
    allResolvable platform('org.springframework.boot:spring-boot-dependencies:2.2.6.RELEASE')
}
```
