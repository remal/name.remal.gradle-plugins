This plugin adds `forBuildOnCi` and `forBuildOnLocal` methods to the project.

```gradle
forBuildOnCI {
    // this code will be executed if current build is executed on a CI server
}
forBuildOnLocal {
    // this code will be executed if current build is NOT executed on a CI server
}
```

This plugin creates `ci` extension of type [`CIExtension`](#nameremalgradle_pluginspluginsciciextension)

Also, this plugin configures Gradle to show stacktraces if current build is executed on a CI server.

&nbsp;

### `name.remal.gradle_plugins.plugins.ci.CIExtension`
| Property | Type | Description
| --- | :---: | --- |
| `pipelineId` | `String?` | Current build pipeline ID. |
| `buildId` | `String?` | Current build ID. |
| `stageName` | `String?` | Current stage name. |
| `jobName` | `String?` | Current job name. |
