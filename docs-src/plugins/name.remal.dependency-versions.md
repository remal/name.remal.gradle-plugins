This plugin creates 'dependencyVersions' extension of type [`DependencyVersionsExtension`](#nameremalgradle_pluginspluginsdependenciesdependencyversionsextension).

The plugin applies `substitute`, `deny` and `reject` rules from [Nebula Gradle Resolution Rules](https://github.com/nebula-plugins/gradle-resolution-rules).

Also it configures preferring dynamic first-level dependencies without invalid version tokens.

&nbsp;

### `name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsExtension`
| Property | Type | Description
| --- | :---: | --- |
| `rejectVersions` | <nobr>`MutableSet<String>`</nobr> | If a dependency version matches at least one pattern in this set, this version will be rejected regardless values in `allowAllVersionsFor` set. |
| `allowAllVersionsFor` | <nobr>`MutableSet<String>`</nobr> | Dependency notation patterns to allow all versions (without invalid tokens check). |
| `invalidVersionTokens` | <nobr>`MutableSet<String>`</nobr> | If a version has one of these tokens then the version will be skipped. Default value: `alpha`, `a`, `beta`, `b`, `rc`, `cr`, `ea`, `milestone`, `m`, `redhat`, `pr`, `dev`, `snapshot`. |

| Method | Description
| --- | --- |
| `void rejectVersions(String notation)` | Adds `notationPattern` to `rejectVersions`. |
| `void rejectVersions(String... notationPatterns)` | Adds `notationPatterns` to `rejectVersions`. |
| `void allowAllVersionsFor(String notation)` | Adds `notationPattern` to `allowAllVersionsFor`. |
| `void allowAllVersionsFor(String... notationPatterns)` | Adds `notationPatterns` to `allowAllVersionsFor`. |
| `void invalidVersionToken(String token)` | Adds `token` to `invalidVersionTokens`. |
| `void invalidVersionToken(String... tokens)` | Adds `tokens` to `invalidVersionTokens`. |
| `String? getFirstInvalidToken(String version)` | Returns the first invalid version token that the version contains. Returns `null` is the version doesn't contain and invalid version token. |
| `String resolveLatestVersion(String notation)` | Returns the latest version of dependency defined by the `notation`. Versions that contain invalid version tokens are excluded. |
| `List<String> resolveAllVersions(String notation)` | Returns all versions of dependency defined by the `notation`. Versions that contain invalid version tokens are excluded. |
