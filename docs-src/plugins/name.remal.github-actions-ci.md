**This plugin works only if `CI` and `GITHUB_ACTIONS` environment variables equals to `true`.**

The plugin applies [`name.remal.common-ci`](name.remal.common-ci.md) plugin.

Current build is treated as executed on a CI server if `CI` and `GITHUB_ACTIONS` environment variables equals to `true`.

&nbsp;

The plugin sets [`ci`](name.remal.common-ci.md#nameremalgradle_pluginspluginsciciextension) extension values:

* `pipelineId` to `GITHUB_RUN_ID` environment variable
* `buildId` to `null`
* `stageName` to `null`
* `jobName` to `null`

&nbsp;

## If [`name.remal.vcs-operations`](name.remal.vcs-operations.md) plugin is applied, this plugin:

* Sets `vcsOperations.overwriteCurrentBranch` to `GITHUB_REF` (without `ref/heads/` prefix).
