**This plugin works only if [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) plugin is applied.**

The plugin applies [`name.remal.maven-publish-settings`](name.remal.maven-publish-settings.md) and [`name.remal.environment-variables`](name.remal.environment-variables.md) plugins.

&nbsp;

This plugin adds `publishing.repositories.ossrh` method. This method adds [OSSRH](https://central.sonatype.org/pages/ossrh-guide.html) Maven repository to publish Maven artifacts to.

SNAPSHOT publishing is supported.

Once the method is invoked, these plugins are applied:

* [`signing`](https://docs.gradle.org/current/userguide/signing_plugin.html)
* [`name.remal.signing-settings`](name.remal.signing-settings.md)
* [`name.remal.maven-publish-nexus-staging`](name.remal.maven-publish-nexus-staging.md)

Usage:

```groovy tab="Groovy"
publishing.repositories.ossrh {
    credentials.username = "user" // Optional. By default 'OSSRH_USER' or 'OSS_USER' environment variables are used
    credentials.password = "password" // Optional. By default 'OSSRH_PASSWORD' or 'OSS_PASSWORD_USER' environment variables are used
}
```

```kotlin tab="Kotlin"
import name.remal.gradle_plugins.plugins.publish.ossrh.RepositoryHandlerOssrhExtension
import name.remal.gradle_plugins.dsl.extensions.*

publishing.repositories.convention[RepositoryHandlerOssrhExtension::class.java].ossrh {
    credentials.username = "user" // Optional. By default 'OSSRH_USER' or 'OSS_USER' environment variables are used
    credentials.password = "password" // Optional. By default 'OSSRH_PASSWORD' or 'OSS_PASSWORD_USER' environment variables are used
}
```
