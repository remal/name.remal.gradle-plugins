The plugin applies these plugins:

* [`java`](https://docs.gradle.org/current/userguide/java_plugin.html)
* [`name.remal.common-settings`](name.remal.common-settings.md)

&nbsp;

The plugin makes easier to use Java annotation processors. It configures [JavaCompile](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/compile/JavaCompile.html) tasks and sets up a dir for generated sources by annotation-processors.

The plugin creates `aptOnly` configuration. For Gradle version 4.6 and above all `annotationProcessor` configurations extend this configuration. For older Gradle versions all `compileOnly` configurations extend this configuration.

Also, the plugin creates `apt` configuration. This configuration extends `aptOnly`. All `compileOnly` configurations extend this configuration. For Gradle version 4.6 and above all `annotationProcessor` configurations extend this configuration.

&nbsp;

<a id="apt-options"></a>
The plugin adds `aptOptions` extension of type [`AptOptions`](#nameremalgradle_pluginspluginsjavaaptoptions).

&nbsp;

### `name.remal.gradle_plugins.plugins.java.AptOptions`

| Property | Type | Description
| --- | :---: | --- |
| `processorArgs` | <nobr>`MutableMap<String, Any?>`</nobr> | Annotation processors arguments. They will be added as command-line arguments like `-A<key>=<value>` |
