**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies these plugins:

* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.merge-resources`](name.remal.merge-resources.md)

&nbsp;

This plugin makes `jar` task to produce fat-jar of dependencies in `fatJar` configuration. Also it creates `fatJarSettings` extension of type [`FatJarSettingsExtension`](#nameremalgradle_pluginspluginsfatjarfatjarsettingsextension) to configure extraction.

&nbsp;

### `name.remal.gradle_plugins.plugins.fatjar.FatJarSettingsExtension`
| Property | Type | Description
| --- | :---: | --- |
| `excludePatterns` | <nobr>`MutableSet<String>`</nobr> | ANT like exclude patterns of resources from fat-jar dependencies. |
| `excludeSpecs` | <nobr><code>MutableList&lt;<a href="https://docs.gradle.org/current/javadoc/org/gradle/api/specs/Spec.html">Spec</a>&lt;<a href="https://docs.gradle.org/current/javadoc/org/gradle/api/file/FileTreeElement.html">FileTreeElement</a>&gt;&gt;</code></nobr> | Exclude specs of resources from fat-jar dependencies. |

| Method | Description
| --- | --- |
| `void exclude(String... excludes)` | Add ANT like exclude patterns of resources from fat-jar dependencies. |
| `void exclude(Iterable<String> excludes)` | ANT like exclude patterns of resources from fat-jar dependencies. |
| `void exclude(Spec<FileTreeElement> excludeSpec)` | Exclude spec of resources from fat-jar dependencies. |
