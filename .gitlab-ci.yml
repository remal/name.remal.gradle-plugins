image: anapsix/alpine-java:8_jdk_unlimited

before_script:
- java -version || true
- python -V || true
- export GRADLE_OPTS="-Dorg.gradle.daemon=false -Dorg.gradle.parallel=true -Dorg.gradle.workers.max=4 -Dorg.gradle.warning.mode=all -Dsun.net.client.defaultConnectTimeout=15000 -Dsun.net.client.defaultReadTimeout=600000 -Dsun.io.useCanonCaches=false -Djava.awt.headless=true -Dorg.gradle.internal.launcher.welcomeMessageEnabled=false"
- export GRADLE_USER_HOME=`pwd`/.gradle-cache
- mkdir -p "$GRADLE_USER_HOME"; cp -arv "$HOME/.gradle/." "$GRADLE_USER_HOME" || true
- sed -i 's/-all\.zip/-bin.zip/' gradle/wrapper/gradle-wrapper.properties
- command -v java && ./gradlew --quiet --stop &> /dev/null || true

after_script:
- command -v java && ./gradlew --quiet --stop &> /dev/null || true

stages:
- build
- cross-gradle-versions-check
- create-auto-vcs-version-tag
- publish
- pages
- renovate


.default-cache-params: &default-cache-params
  key: $CI_PIPELINE_ID
  untracked: true
  paths:
  - .gradle/
  - .gradle-cache/

.pull-push-cache: &pull-push-cache
  cache:
    <<: *default-cache-params

.push-cache: &push-cache
  cache:
    <<: *default-cache-params
    policy: push

.pull-cache: &pull-cache
  cache:
    <<: *default-cache-params
    policy: pull


build-develop:
  only:
    refs:
    - develop
    variables:
    - $PERIODIC_BUILD
  except:
    variables:
    - $RENOVATE
  stage: build
  cache:
    <<: *default-cache-params
    key: periodic-build-${CI_COMMIT_REF_NAME}
    untracked: false
    when: always
  interruptible: true
  script:
  - ./retry ./gradlew clean
  - ./retry ./gradlew downloadDependencies
  - ./gradlew allClasses
  - ./gradlew build -Pdisable-compilation=true


build:
  only:
  - master
  stage: build
  <<: *push-cache
  interruptible: true
  resource_group: build-master
  script:
  - ./retry ./gradlew clean
  - ./retry ./gradlew downloadDependencies
  - ./gradlew writeAutoVcsVersionInFile
  - ./gradlew failIfProjectVersionTagExists
  - ./gradlew allClasses
  - ./gradlew build allTests -Pdisable-compilation=true
  - ./gradlew processDocs generateDocsInclude -Pdisable-tests=true -Pdisable-compilation=true

#cross-gradle-versions-check:
#  only:
#  - master
#  stage: cross-gradle-versions-check
#  <<: *pull-cache
#  interruptible: true
#  parallel: 10
#  script:
#  - ./gradlew tasks --group=verification -Pgradle.cross-versions.number=$CI_NODE_INDEX -Pgradle.cross-versions.total=$CI_NODE_TOTAL
#  - ./gradlew crossGradleVersionsCheck -Pgradle.cross-versions.number=$CI_NODE_INDEX -Pgradle.cross-versions.total=$CI_NODE_TOTAL -Pdisable-compilation=true -Pdisable-static-analysis=true -x test -x testSimpleAutoPluginTests

create-auto-vcs-version-tag:
  only:
  - master
  stage: create-auto-vcs-version-tag
  <<: *pull-push-cache
  interruptible: false
  resource_group: build-master
  script:
  - 'if [ ! -z "$SKIP_PUBLISHING" ]; then >&2 echo "!!! SKIP PUBLISHING !!!"; exit 1; fi'
  - ./gradlew createAutoVcsVersionTag

#publish-to-bintray:
#  only:
#  - master
#  stage: publish
#  <<: *pull-cache
#  interruptible: false
#  resource_group: build-master
#  script:
#  - ./gradlew publishToBintray -Pdisable-tests=true -Pdisable-compilation=true

publish-to-ossrh:
  only:
  - master
  stage: publish
  <<: *pull-cache
  interruptible: false
  resource_group: build-master
  script:
  - ./gradlew publishToSonatype closeAndReleaseSonatypeStagingRepository -Pdisable-tests=true -Pdisable-compilation=true

#publish-to-gradle-plugin-portal:
#  only:
#  - master
#  stage: publish
#  <<: *pull-cache
#  interruptible: false
#  resource_group: build-master
#  script:
#  - ./gradlew publishToGradlePluginPortal -Pdisable-tests=true -Pdisable-compilation=true


#process-docs:
#  only:
#  - docs
#  stage: build
#  <<: *push-cache
#  interruptible: true
#  script:
#  - ./gradlew clean
#  - ./gradlew downloadDependencies
#  - ./gradlew assemble
#  - ./gradlew processDocs generateDocsInclude -Pdisable-tests=true -Pdisable-compilation=true
#
#pages:
#  only:
#  - master
#  - docs
#  stage: pages
#  artifacts:
#    paths:
#    - public
#    expire_in: 1 week
#  <<: *pull-cache
#  interruptible: true
#  resource_group: build-master
#  image: python:3.8
#  script:
#  #- 'if ! [ -d "docs-generated" ]; then echo "Creating docs-generated" && mkdir -p docs-generated && echo "" > docs-generated/plugins-index.md; fi'
#  #- 'if ! [ -d "docs" ]; then echo "Creating docs" && cp -r docs-src docs; fi'
#  - >-
#    pip install
#    "mkdocs>=1.1,<1.2"
#    "mkdocs-material>=4.6,<4.7"
#    "markdown-headdown>=0.1,<0.2"
#    "pymdown-extensions>=6.3,<6.4"
#    "pyembed-markdown>=1.1,<1.2"
#    "mdx_include>=1.3,<1.4"
#    "markdown-iconfonts>=3.0,<3.1"
#  - mkdocs build --clean --verbose
#  - mv site public


renovate:
  only:
    refs:
    - develop
    variables:
    - $RENOVATE
  stage: renovate
  interruptible: false
  timeout: 55 minutes
  services:
  - docker:dind
  image: renovate/renovate:slim
  cache:
    key: renovate-${CI_COMMIT_REF_SLUG}
    paths:
    - $CI_PROJECT_DIR/.renovate
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://docker:2375
    RENOVATE_PLATFORM: gitlab
    RENOVATE_BASE_DIR: $CI_PROJECT_DIR/.renovate
    RENOVATE_ENDPOINT: $CI_API_V4_URL
    RENOVATE_GIT_AUTHOR: Renovate Bot <bot@renovateapp.com>
    #RENOVATE_OPTIMIZE_FOR_DISABLED: 'true'
    RENOVATE_REPOSITORY_CACHE: 'true'
    RENOVATE_REQUIRE_CONFIG: 'true'
    RENOVATE_ONBOARDING: 'false'
    RENOVATE_IGNORE_PR_AUTHOR: 'true'
    LOG_LEVEL: debug
  script:
  - renovate "$CI_PROJECT_PATH"
