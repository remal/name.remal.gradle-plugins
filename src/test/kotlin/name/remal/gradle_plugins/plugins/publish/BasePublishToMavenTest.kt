package name.remal.gradle_plugins.plugins.publish

import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_8
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.util.GradleVersion
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class BasePublishToMavenTest : BaseProjectTest() {

    @Before
    fun before() {
        project.applyPlugin(JavaPluginId)
        project.applyPlugin(MavenPublishPluginId)

        project.extensions[PublishingExtension::class.java].publications.create("publication", MavenPublication::class.java) { publication ->
            project.tasks.create("basePublishToMaven", VisibleBasePublishToMaven::class.java) {
                it.publication = publication
            }
        }
    }


    @Test
    fun publicationFilesContainsPomFile() {
        if (GradleVersion.current() < GRADLE_VERSION_4_8) {
            // This task fails on Gradle < 4.8, as 'maven-publish' plugin uses Gradle Rules
            return
        }

        val task = project.tasks[VisibleBasePublishToMaven::class.java, "basePublishToMaven"]
        val pomFile = task.pomFile
        assertNotNull(pomFile)
        assertTrue(pomFile.name.startsWith("pom"))
        assertTrue(pomFile.name.endsWith(".xml"))
    }


    @BuildTask
    class VisibleBasePublishToMaven : BasePublishToMaven() {
        public override val pomFile get() = super.pomFile
    }

}
