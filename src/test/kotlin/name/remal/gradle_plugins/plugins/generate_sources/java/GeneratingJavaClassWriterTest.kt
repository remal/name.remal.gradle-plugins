package name.remal.gradle_plugins.plugins.generate_sources.java

import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test
import java.io.File
import java.io.StringWriter
import java.nio.file.Files

class GeneratingJavaClassWriterTest : BaseProjectTest() {

    private val targetFile: File = File(".").absoluteFile

    private val destinationWriter = StringWriter()

    private val writer = GeneratingJavaClassWriter(
        "pkg",
        "Simple",
        targetFile,
        targetFile.name,
        project.tasks.create("generate", GenerateJava::class.java),
        destinationWriter
    )

    @After
    fun cleanup() {
        writer.close()
    }


    private val writtenText: String
        get() {
            writer.flush()
            return destinationWriter.toString()
        }

    private val writtenLines: List<String>
        get() {
            return writtenText.splitToSequence('\n', '\r').filter(String::isNotEmpty).toList()
        }


    @Test
    fun targetFile() {
        assertThat(writer.targetFile).isEqualTo(targetFile)
    }

    @Test
    fun relativePath() {
        assertThat(writer.relativePath).isEqualTo(targetFile.name)
    }

    @Test
    fun generateTask() {
        assertThat(writer.generateTask).isSameAs(project.tasks["generate"])
    }


    @Test
    fun writeln() {
        writer.writeln()
        writer.writeln(null)
        writer.writeln(1)
        assertThat(writtenText).isEqualTo("\nnull\n1\n")
    }


    @Test
    fun escapeJava() {
        assertThat(writer.escapeJava("\"\n")).isEqualTo("\\\"\\n")
    }

    @Test
    fun escapeRegex() {
        assertThat(writer.escapeRegex(".[A")).isEqualTo("\\.\\[A")
    }

    @Test
    fun writeSuppressWarnings() {
        writer.writeSuppressWarnings()
        writer.writeSuppressWarnings("1")
        writer.writeSuppressWarnings("1", "2")
        assertThat(writtenLines).containsExactly(
            "@SuppressWarnings(\"all\")",
            "@SuppressWarnings(\"1\")",
            "@SuppressWarnings({\"1\", \"2\"})"
        )
    }

    @Test
    fun writePackage() {
        writer.writePackage()
        assertThat(writtenLines).containsExactly(
            "package pkg;"
        )
    }

    @Test
    fun writeImport() {
        writer.writeImport("java.io.File")
        writer.writeImport(StringWriter::class.java)
        assertThat(writtenLines).containsExactly(
            "import java.io.File;",
            "import java.io.StringWriter;"
        )
    }

    @Test
    fun writeStaticImport() {
        writer.writeStaticImport("java.nio.file.Files", "createDirectories")
        writer.writeStaticImport(Files::class.java, "createDirectories")
        writer.writeStaticImport(Files::createDirectories)
        assertThat(writtenLines).containsExactly(
            "import static java.nio.file.Files.createDirectories;",
            "import static java.nio.file.Files.createDirectories;",
            "import static java.nio.file.Files.createDirectories;"
        )
    }

    @Test
    fun writeBlock() {
        writer.writeBlock()
        writer.writeBlock {
            writeln(1)
        }
        writer.writeBlock("expression  \t  ")
        writer.writeBlock("expression") {
            writeln(2)
            writeln("    ")
        }
        writer.writeBlock("wrapper") {
            writeBlock("outer") {
                writeBlock("inner") {
                    writeln(3)
                }
            }
        }
        assertThat(writtenLines).containsExactly(
            "{}",
            "{",
            "    1",
            "}",
            "expression {}",
            "expression {",
            "    2",
            "}",
            "wrapper {",
            "    outer {",
            "        inner {",
            "            3",
            "        }",
            "    }",
            "}"
        )
    }

    @Test
    fun writeOverrideAnnotation() {
        writer.writeOverrideAnnotation()
        assertThat(writtenText).isEqualTo("@Override\n")
    }

}
