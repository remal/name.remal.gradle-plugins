package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_9
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.createWithUniqueName
import name.remal.gradle_plugins.dsl.extensions.selectHighestCapabilityVersion
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.gradle_plugins.testing.dsl.TEST_MAVEN_REPO_DEFAULT_VERSION
import name.remal.gradle_plugins.testing.dsl.testMavenRepository
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ResolveException
import org.gradle.api.artifacts.ResolvedArtifact
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.gradle.util.GradleVersion
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test

class ComponentCapabilitiesPluginTest : BaseProjectTest() {

    private lateinit var conf: Configuration

    @Before
    fun before() {
        project.applyPlugin(ComponentCapabilitiesPlugin::class.java)

        project.testMavenRepository {
            component("asm", "asm", "0.1") { jar() }
            component("asm", "asm", "1") { jar() }
            component("org.ow2.asm", "asm", "1.1") { jar() }
            component("org.ow2.asm", "asm", "2") { jar() }

            val slf4jApi = component("org.slf4j", "slf4j-api") { jar() }
            component("org.slf4j", "slf4j-nop") { pom { dependency(slf4jApi) }; jar() }
            component("org.slf4j", "log4j-over-slf4j") { pom { dependency(slf4jApi) }; jar() }
            component("ch.qos.logback", "logback-classic") { pom { dependency(slf4jApi) }; jar() }

            component("log4j", "log4j") { jar() }
        }

        conf = project.configurations.createWithUniqueName() {
            it.resolutionStrategy.selectHighestCapabilityVersion("org.ow2.asm", "asm")
        }
    }


    @Test
    fun asm() {
        addDependency("asm", "asm", "0.1")
        addDependency("asm", "asm", "1")
        addDependency("org.ow2.asm", "asm", "1.1")
        addDependency("org.ow2.asm", "asm", "2")

        if (GradleVersion.current() < GRADLE_VERSION_4_9) {
            assertEquals(2, resolvedArtifacts.size)

            assertEquals("asm", resolvedArtifacts[0].group)
            assertEquals("asm", resolvedArtifacts[0].module)
            assertEquals("1", resolvedArtifacts[0].version)

            assertEquals("org.ow2.asm", resolvedArtifacts[1].group)
            assertEquals("asm", resolvedArtifacts[1].module)
            assertEquals("2", resolvedArtifacts[1].version)

        } else {
            assertEquals(1, resolvedArtifacts.size)

            assertEquals("org.ow2.asm", resolvedArtifacts[0].group)
            assertEquals("asm", resolvedArtifacts[0].module)
            assertEquals("2", resolvedArtifacts[0].version)
        }
    }


    @Test
    fun slf4jImpl() {
        addDependency("org.slf4j", "slf4j-nop")
        addDependency("ch.qos.logback", "logback-classic")

        if (GradleVersion.current() < GRADLE_VERSION_4_9) {
            assertEquals(3, resolvedArtifacts.size)

            assertEquals("ch.qos.logback", resolvedArtifacts[0].group)
            assertEquals("logback-classic", resolvedArtifacts[0].module)

            assertEquals("org.slf4j", resolvedArtifacts[1].group)
            assertEquals("slf4j-api", resolvedArtifacts[1].module)

            assertEquals("org.slf4j", resolvedArtifacts[2].group)
            assertEquals("slf4j-nop", resolvedArtifacts[2].module)

        } else {
            try {
                resolvedArtifacts
                fail()
            } catch (e: ResolveException) {
                // OK
            }
        }
    }


    @Test
    fun log4j12Api() {
        addDependency("org.slf4j", "log4j-over-slf4j")
        addDependency("log4j", "log4j")

        if (GradleVersion.current() < GRADLE_VERSION_4_9) {
            assertEquals(3, resolvedArtifacts.size)

            assertEquals("log4j", resolvedArtifacts[0].group)
            assertEquals("log4j", resolvedArtifacts[0].module)

            assertEquals("org.slf4j", resolvedArtifacts[1].group)
            assertEquals("log4j-over-slf4j", resolvedArtifacts[1].module)

            assertEquals("org.slf4j", resolvedArtifacts[2].group)
            assertEquals("slf4j-api", resolvedArtifacts[2].module)

        } else {
            try {
                resolvedArtifacts
                fail()
            } catch (e: ResolveException) {
                // OK
            }
        }
    }


    private fun addDependency(group: String, module: String, version: String = TEST_MAVEN_REPO_DEFAULT_VERSION) {
        conf.dependencies.add(project.dependencies.create("$group:$module:$version"))
    }

    private val ResolvedArtifact.group get() = (id.componentIdentifier as ModuleComponentIdentifier).group
    private val ResolvedArtifact.module get() = (id.componentIdentifier as ModuleComponentIdentifier).module
    private val ResolvedArtifact.version get() = (id.componentIdentifier as ModuleComponentIdentifier).version

    private val resolvedArtifacts: List<ResolvedArtifact>
        get() = conf.resolvedConfiguration.resolvedArtifacts.sortedWith(Comparator { a1, a2 ->
            a1.group.compareTo(a2.group).let { if (it != 0) return@Comparator it }
            a1.module.compareTo(a2.module).let { if (it != 0) return@Comparator it }
            a1.version.compareTo(a2.version).let { if (it != 0) return@Comparator it }
            return@Comparator 0
        })

}
