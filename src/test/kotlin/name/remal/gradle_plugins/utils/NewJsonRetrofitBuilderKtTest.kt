package name.remal.gradle_plugins.utils

import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.ZoneId
import java.time.ZonedDateTime

class NewJsonRetrofitBuilderKtTest {

    @Test
    fun parseWithRetrofitJsonObjectMapper() {
        val dateTime = __parseWithRetrofitJsonObjectMapper('"' + "2019-03-15 13:39:55.3 UTC" + '"', ZonedDateTime::class.java)
        assertEquals(
            ZonedDateTime.of(2019, 3, 15, 13, 39, 55, 300_000_000, ZoneId.of("UTC")),
            dateTime
        )
    }

}
