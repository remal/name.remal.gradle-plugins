package name.remal.gradle_plugins.plugins.vcs

import name.remal.createParentDirectories
import name.remal.gradle_plugins.dsl.BuildTask
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import java.io.File

@BuildTask
class WriteAutoVcsVersionInFileTask : DefaultTask() {

    @get:OutputFile
    var file: File = project.buildDir.resolve("auto-vcs-version.txt")

    @get:Input
    protected val version: String
        get() = project.version.toString()

    @TaskAction
    protected fun doWriteAutoVcsVersionInFile() {
        logger.lifecycle("Writing version '{}' to file: {}", version, file)
        file.createParentDirectories().writeText(version)
        didWork = true
    }

}
