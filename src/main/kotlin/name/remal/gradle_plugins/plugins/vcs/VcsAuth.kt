package name.remal.gradle_plugins.plugins.vcs

import java.io.File

sealed class AbstractVcsAuth

class UsernamePasswordVcsAuth @JvmOverloads constructor(val username: String, val password: CharArray = charArrayOf()) : AbstractVcsAuth() {
    constructor(username: String, password: String) : this(username, password.toCharArray())
}

class SSHVcsAuth @JvmOverloads constructor(val privateKeyFile: File, val password: CharArray? = null) : AbstractVcsAuth() {
    constructor(privateKeyFile: File, password: String?) : this(privateKeyFile, password?.toCharArray())
}
