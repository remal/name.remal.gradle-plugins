package name.remal.gradle_plugins.plugins.vcs

class GitOperationException : VcsOperationException {
    constructor(message: String) : super("GIT: $message")
    constructor(message: String, cause: Throwable) : super("GIT: $message", cause)
    constructor(cause: Throwable) : super("GIT operation exception", cause)
}
