package name.remal.gradle_plugins.plugins.java

import name.remal.gradle_plugins.dsl.PluginId
import name.remal.gradle_plugins.dsl.utils.findPluginId
import org.gradle.api.plugins.JavaBasePlugin

object JavaBasePluginId : PluginId(
    "java-base",
    findPluginId(JavaBasePlugin::class.java)?.let(::listOf) ?: emptyList()
)
