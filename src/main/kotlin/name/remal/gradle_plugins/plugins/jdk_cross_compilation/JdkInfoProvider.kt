package name.remal.gradle_plugins.plugins.jdk_cross_compilation

import name.remal.Ordered
import org.gradle.api.JavaVersion

/*
 * See provider examples: https://github.com/nebula-plugins/gradle-java-cross-compile-plugin/tree/master/src/main/kotlin/nebula/plugin/compile/provider
 */

interface JdkInfoProvider : Ordered<JdkInfoProvider> {

    fun provide(javaVersion: JavaVersion): JdkInfo?

}
