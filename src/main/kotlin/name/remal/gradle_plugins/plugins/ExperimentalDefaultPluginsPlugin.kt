package name.remal.gradle_plugins.plugins

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin

@Plugin(
    id = "name.remal.experimental-default-plugins",
    description = "Plugin that applies name.remal.default-plugins plugin and default experimental plugins (see full docs for a list of the plugins).",
    tags = ["common", "default", "experimental"]
)
@ApplyPluginClasses(
    DefaultPluginsPlugin::class
)
class ExperimentalDefaultPluginsPlugin : BaseReflectiveProjectPlugin()
