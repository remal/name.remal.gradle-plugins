package name.remal.gradle_plugins.plugins.null_checks

import name.remal.accept
import name.remal.allAnnotations
import name.remal.buildList
import name.remal.concurrentMapOf
import name.remal.default
import name.remal.get
import name.remal.getAllParameterAnnotations
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassInternalName
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.POST_PROCESSING_STAGE
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.dsl.utils.ClassInternalName
import name.remal.gradle_plugins.plugins.null_checks.InsertNullChecksProcessor.NullabilityMode.NON_NULL
import name.remal.gradle_plugins.plugins.null_checks.InsertNullChecksProcessor.NullabilityMode.NULLABLE
import name.remal.gradle_plugins.plugins.null_checks.InsertNullChecksProcessor.NullabilityMode.UNKNOWN_NULLABILITY
import name.remal.hasParameters
import name.remal.isAbstract
import name.remal.isAnnotation
import name.remal.isEnum
import name.remal.isKotlinClass
import name.remal.isPrimitive
import name.remal.isPrivate
import name.remal.isStatic
import name.remal.isSynthetic
import name.remal.nullIfEmpty
import org.codehaus.groovy.runtime.GeneratedClosure
import org.gradle.api.tasks.compile.AbstractCompile
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassReader.SKIP_CODE
import org.objectweb.asm.ClassReader.SKIP_DEBUG
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.ClassWriter.COMPUTE_MAXS
import org.objectweb.asm.Opcodes.ALOAD
import org.objectweb.asm.Opcodes.ILOAD
import org.objectweb.asm.Opcodes.INVOKESPECIAL
import org.objectweb.asm.Opcodes.INVOKESTATIC
import org.objectweb.asm.Opcodes.IRETURN
import org.objectweb.asm.Opcodes.POP
import org.objectweb.asm.Type
import org.objectweb.asm.Type.getArgumentTypes
import org.objectweb.asm.Type.getInternalName
import org.objectweb.asm.Type.getMethodDescriptor
import org.objectweb.asm.Type.getReturnType
import org.objectweb.asm.Type.getType
import org.objectweb.asm.tree.AbstractInsnNode
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.InnerClassNode
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.InsnNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.LineNumberNode
import org.objectweb.asm.tree.MethodInsnNode
import org.objectweb.asm.tree.MethodNode
import org.objectweb.asm.tree.VarInsnNode
import java.lang.annotation.ElementType
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import javax.annotation.Nonnull
import javax.annotation.meta.TypeQualifierDefault
import javax.annotation.meta.TypeQualifierNickname
import java.lang.annotation.Repeatable as JavaRepeatable

class InsertNullChecksProcessor : ClassesProcessor {

    companion object {
        private const val addNullChecksToGeneratedMethods = true
        private const val addNullChecksToPrivateMethods = false

        private val skipAnnotationDescrs: Set<String> = hashSetOf(
            "Lkotlin/Metadata;", // Kotlin language handles NULL itself
            "Lorg/immutables/value/Generated;" // Immutables library handles NULL itself
        )

        /**
         * See [https://checkerframework.org/manual/#nullness-related-work]
         */
        private val nullableSimpleClassNames: Set<String> = hashSetOf(
            "Nullable",
            "RecentlyNullable",
            "CheckForNull",
            "PossiblyNull",
            "UnknownNullness",
            "NullableDecl",
            "NullableType",
            "NullAllowed",
            "NullUnknown"
        )

        /**
         * See [https://checkerframework.org/manual/#nullness-related-work]
         */
        private val nonNullSimpleClassNames: Set<String> = hashSetOf(
            "NonNull",
            "RecentlyNonNull",
            "NotNull",
            "Nonnull",
            "NonNullDecl",
            "NonNullType"
        )

        private val nonNullWithGeneratedChecksClassDescrs: Set<String> = hashSetOf(
            "Llombok/NonNull;"
        )

        private val getterMethodNameRegEx = Regex("(?:get|is)[\\p{Lu}_\\d].*")
        private val builderClassNameRegEx = Regex("(.*[\\p{Ll}_\\d])?Builder([\\p{Lu}_\\d].*)?")
    }

    @Suppress("LongMethod", "ComplexMethod", "ReturnCount", "kotlin:S3776")
    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        val classNode = ClassNode()
        val classReader = ClassReader(bytecode)
        classReader.accept(classNode)

        val shouldClassBeSkipped: Boolean = classNode.run shouldBeSkipped@{
            if (!outerMethod.isNullOrEmpty()) {
                return@shouldBeSkipped true
            }

            if (!addNullChecksToGeneratedMethods) {
                val isGeneratedClass = sequenceOf(visibleAnnotations, invisibleAnnotations)
                    .filterNotNull()
                    .flatten()
                    .map(AnnotationNode::desc)
                    .map { it.substring(1, it.length - 1).substringAfter('/') }
                    .any { it == "Generated" }
                if (isGeneratedClass) {
                    return@shouldBeSkipped true
                }
            }

            if (isKotlinClass
                || isGroovyGeneratedClosureClass
            ) {
                return@shouldBeSkipped true
            }

            val isSkippedByAnnotations = sequenceOf(visibleAnnotations, invisibleAnnotations)
                .filterNotNull()
                .flatten()
                .any { it.desc in skipAnnotationDescrs }
            if (isSkippedByAnnotations) {
                return@shouldBeSkipped true
            }

            if (context.hasValidationAnnotations(this)) {
                return@shouldBeSkipped true
            }

            return@shouldBeSkipped false
        }
        if (shouldClassBeSkipped) {
            return
        }

        val methods = classNode.methods.default().asSequence()
            .filterNot(MethodNode::isSynthetic)
            .filterNot(MethodNode::isAbstract)
            .filter { !it.isPrivate || addNullChecksToPrivateMethods }
            .filter {
                sequenceOf(it.visibleAnnotations, it.invisibleAnnotations)
                    .filterNotNull()
                    .flatten()
                    .none { it.desc in skipAnnotationDescrs }
            }
            .filter {
                getArgumentTypes(it.desc).run {
                    isNotEmpty() || all { it.isPrimitive }
                }
            }
            .filter {
                if (it.isStatic) {
                    if (classNode.isEnum && it.name == "valueOf" && it.desc == "(Ljava/lang/String;)L${classNode.name};") return@filter false

                } else {
                    if (it.name == "equals" && it.desc == "(Ljava/lang/Object;)Z") return@filter false
                }

                return@filter true
            }
            .toList()
            .nullIfEmpty() ?: return

        var isModified = false
        methods.forEach forEachMethod@{ method ->
            val filteredInstructions = method.instructions.default(emptyList()).filter { it !is LineNumberNode }
            if (filteredInstructions.isEmpty()) {
                /*
                 * It's a very strange situation, as all methods are expected to have at least one return instruction.
                 * Let's skip the method, as we don't want to break anything in its bytecode.
                 */
                return@forEachMethod
            }

            run canBeSkipped@{
                val instructionsWithoutLabels = filteredInstructions.filter { it !is LabelNode }
                if (instructionsWithoutLabels.isEmpty()) {
                    /*
                     * It's a very strange situation, as all methods are expected to have at least one non-label instruction.
                     * Let's skip the method, as we don't want to break anything in its bytecode.
                     */
                    return@forEachMethod
                }

                if (!addNullChecksToGeneratedMethods) {
                    val isGeneratedMethod = sequenceOf(method.visibleAnnotations, method.invisibleAnnotations)
                        .filterNotNull()
                        .flatten()
                        .map(AnnotationNode::desc)
                        .map { it.substring(1, it.length - 1).substringAfter('/') }
                        .any { it == "Generated" }
                    if (isGeneratedMethod) {
                        return@forEachMethod
                    }
                }

                val doesOnlyCallSuperMatchers = buildList<((insn: AbstractInsnNode) -> Boolean)> {
                    if (!method.isStatic) {
                        add({ insn -> insn is VarInsnNode && insn.opcode == ALOAD && insn.`var` == 0 })
                    }

                    var paramIndex = if (method.isStatic) 0 else 1
                    getArgumentTypes(method.desc).forEach { type ->
                        val curParamIndex = paramIndex
                        add({ insn -> insn is VarInsnNode && insn.opcode == type.getOpcode(ILOAD) && insn.`var` == curParamIndex })
                        paramIndex += type.size
                    }

                    add(matcher@{ insn ->
                        if (insn !is MethodInsnNode) return@matcher false
                        if (method.isStatic && insn.opcode != INVOKESTATIC) return@matcher false
                        if (method.isConstructor && insn.opcode != INVOKESPECIAL) return@matcher false
                        return@matcher insn.owner == classNode.superName && insn.name == method.name && insn.desc == method.desc
                    })

                    add({ insn -> insn is InsnNode && insn.opcode == getReturnType(method.desc).getOpcode(IRETURN) })
                }
                val doesOnlyCallSuper = run {
                    instructionsWithoutLabels.forEachIndexed { index, insn ->
                        val matcher = doesOnlyCallSuperMatchers.getOrNull(index) ?: return@run false
                        if (!matcher(insn)) return@run false
                    }
                    return@run true
                }
                if (doesOnlyCallSuper) {
                    return@forEachMethod
                }
            }

            val candidateNonNullParamIndexes = method.candidateNonNullParamIndexes
            val paramsAnnotations = candidateNonNullParamIndexes.associate { it to method.getAllParameterAnnotations(it) }
            val nonNullParamIndexes = candidateNonNullParamIndexes.filter { index ->
                val annotations = paramsAnnotations[index] ?: emptyList()
                if (annotations.any { it.isNonNullDirectly }) return@filter true
                if (annotations.any { context.isNonNullIndirectly(it) }) return@filter true
                if (context.areParametersNonNullByDefault(classNode)) return@filter true
                return@filter false
            }
            if (nonNullParamIndexes.isEmpty()) return@forEachMethod

            val insnList = InsnList().apply {
                add(LabelNode())
                val argumentTypes = getArgumentTypes(method.desc)
                nonNullParamIndexes.forEach forEachParamIndex@{ index ->
                    var bytecodeIndex = 0
                    if (!method.isStatic) bytecodeIndex += 1
                    argumentTypes.asSequence()
                        .take(index)
                        .forEach { bytecodeIndex += it.size }

                    add(VarInsnNode(argumentTypes[index].getOpcode(ILOAD), bytecodeIndex))
                    add(LdcInsnNode(buildString {
                        append(
                            method.parameters?.getOrNull(index)?.name.nullIfEmpty() ?: "arg${index + 1}"
                        )
                        append(" must not be null")
                    }))
                    add(
                        MethodInsnNode(
                            INVOKESTATIC,
                            getInternalName(Objects::class.java),
                            "requireNonNull",
                            getMethodDescriptor(getType(Any::class.java), getType(Any::class.java), getType(String::class.java)),
                            false
                        )
                    )
                    add(InsnNode(POP))
                }
            }

            val previousInsn: AbstractInsnNode? = run {
                if (method.name == "<init>") {
                    return@run filteredInstructions.firstOrNull filter@{ insn ->
                        if (insn is MethodInsnNode) {
                            if (insn.name == "<init>"
                                && (insn.owner == classNode.name || insn.owner == classNode.superName)
                            ) {
                                return@filter true
                            }
                        }
                        return@filter false
                    }
                }

                return@run null
            }
            if (previousInsn != null) {
                method.instructions.insert(previousInsn, insnList)
            } else {
                method.instructions.insert(insnList)
            }

            isModified = true
        }

        if (isModified) {
            val classWriter = ClassWriter(COMPUTE_MAXS)
            classNode.accept(classWriter)
            bytecodeModifier.modify(classWriter.toByteArray())
        }
    }


    private enum class NullabilityMode {
        NON_NULL,
        NULLABLE,
        UNKNOWN_NULLABILITY
    }

    private val parametersDefaultNullabilityAnnotations: ConcurrentMap<String, NullabilityMode> = ConcurrentHashMap()
    private val parametersDefaultNullabilityClasses: ConcurrentMap<String, NullabilityMode> = ConcurrentHashMap()
    private val parametersDefaultNullabilityPackages: ConcurrentMap<String, NullabilityMode> = ConcurrentHashMap()
    private val indirectNullabilityAnnotations: ConcurrentMap<String, NullabilityMode> = ConcurrentHashMap()

    @Suppress("kotlin:S3776")
    private fun ProcessContext.getParametersDefaultNullability(annotation: AnnotationNode): NullabilityMode {
        return parametersDefaultNullabilityAnnotations.computeIfAbsent(
            descriptorToClassInternalName(annotation),
            compute@{ classInternalName ->
                if (classInternalName.startsWith("java/")) {
                    return@compute UNKNOWN_NULLABILITY
                }

                if (classInternalName == "org/eclipse/jdt/annotation/NonNullByDefault") {
                    return@compute NON_NULL
                }

                val annotationClassNode = parseBytecode(classInternalName, SKIP_CODE or SKIP_DEBUG)
                    ?: return@compute UNKNOWN_NULLABILITY

                val allAnnotations = annotationClassNode.allAnnotations
                val isTargettingParameters = run {
                    val targetAnnotation = allAnnotations.firstOrNull { it.desc == getClassDescriptor(TypeQualifierDefault::class.java) }
                    val targets = targetAnnotation?.get("value")
                    if (targets !is List<*>) {
                        return@run false
                    }
                    return@run targets.any { target ->
                        target is Array<*> && target[1] == ElementType.PARAMETER.name
                    }
                }
                if (isTargettingParameters) {
                    val nonNullAnnotation = allAnnotations.firstOrNull { it.desc == getClassDescriptor(Nonnull::class.java) }
                    if (nonNullAnnotation != null) {
                        val whenValue = nonNullAnnotation["when"]
                        if (whenValue !is Array<*>) {
                            return@compute NON_NULL
                        }
                        return@compute when (whenValue[1].toString()) {
                            "ALWAYS" -> NON_NULL
                            "UNKNOWN" -> NULLABLE
                            "NEVER" -> NULLABLE
                            else -> UNKNOWN_NULLABILITY
                        }
                    }
                }

                return@compute UNKNOWN_NULLABILITY
            }
        )
    }

    @Suppress("kotlin:S3776")
    private fun ProcessContext.isNonNullIndirectly(annotation: AnnotationNode): Boolean {
        val nullabilityMode = indirectNullabilityAnnotations.computeIfAbsent(
            descriptorToClassInternalName(annotation),
            compute@{ classInternalName ->
                if (classInternalName.startsWith("java/")) {
                    return@compute UNKNOWN_NULLABILITY
                }

                val annotationClassNode = parseBytecode(classInternalName, SKIP_CODE or SKIP_DEBUG)
                    ?: return@compute UNKNOWN_NULLABILITY

                val allAnnotations = annotationClassNode.allAnnotations
                if (allAnnotations.any { it.desc == getClassDescriptor(TypeQualifierNickname::class.java) }) {
                    val nonNullAnnotation = allAnnotations.firstOrNull { it.desc == getClassDescriptor(Nonnull::class.java) }
                    if (nonNullAnnotation != null) {
                        val whenValue = nonNullAnnotation["when"]
                        if (whenValue !is Array<*>) {
                            return@compute NON_NULL
                        }
                        if (whenValue[1].toString() == "ALWAYS") {
                            return@compute NON_NULL
                        } else {
                            return@compute NULLABLE
                        }
                    }
                }

                return@compute UNKNOWN_NULLABILITY
            })
        return nullabilityMode == NON_NULL
    }

    private fun ProcessContext.areParametersNonNullByDefault(classNode: ClassNode): Boolean {
        val className = classNode.name.replace('/', '.')
        val parametersDefaultNullabilityClass = parametersDefaultNullabilityClasses.computeIfAbsent(
            className,
            compute@{ _ ->
                return@compute classNode.allAnnotations.asSequence()
                    .map { getParametersDefaultNullability(it) }
                    .distinct()
                    .sorted()
                    .firstOrNull()
                    ?: UNKNOWN_NULLABILITY
            }
        )
        if (parametersDefaultNullabilityClass == NON_NULL) {
            return true
        }

        return areParametersNonNullByDefaultForPackage(className.substringBeforeLast('.', ""))
    }

    private fun ProcessContext.areParametersNonNullByDefaultForPackage(packageName: String): Boolean {
        val parametersDefaultNullabilityPackage = parametersDefaultNullabilityPackages.computeIfAbsent(
            packageName,
            compute@{ _ ->
                val packageInfoNode = parseBytecode(
                    "${packageName.replace('.', '/')}/package-info".trim('/'),
                    SKIP_CODE or SKIP_DEBUG
                ) ?: return@compute UNKNOWN_NULLABILITY

                return@compute packageInfoNode.allAnnotations.asSequence()
                    .map { getParametersDefaultNullability(it) }
                    .distinct()
                    .sorted()
                    .firstOrNull()
                    ?: UNKNOWN_NULLABILITY
            }
        )
        return parametersDefaultNullabilityPackage == NON_NULL
    }


    private val ClassNode.isGroovyGeneratedClosureClass: Boolean
        get() = interfaces?.contains(getClassInternalName(GeneratedClosure::class.java)) ?: false


    private val MethodNode.candidateNonNullParamIndexes: List<Int>
        get() = buildList {
            val paramTypes = getArgumentTypes(desc)
            paramTypes.forEachIndexed { index, type ->
                if (type.isPrimitive) return@forEachIndexed
                if (parameters?.getOrNull(index)?.isSynthetic == true) return@forEachIndexed
                val allParameterAnnotations = getAllParameterAnnotations(index)
                if (allParameterAnnotations.any { it.desc in nonNullWithGeneratedChecksClassDescrs }) return@forEachIndexed
                if (allParameterAnnotations.any { it.isNullableDirectly }) return@forEachIndexed
                add(index)
            }
        }

    private val AnnotationNode.isNullableDirectly: Boolean
        get() {
            if (desc == getClassDescriptor(Nonnull::class.java)) {
                val whenValue = get("when") ?: return false
                if (whenValue is Array<*>) {
                    return whenValue[1].toString() != "ALWAYS"
                }
            }
            return desc.substring(1, desc.length - 1).substringAfterLast('/') in nullableSimpleClassNames
        }

    private val AnnotationNode.isNonNullDirectly: Boolean
        get() {
            if (isNullableDirectly) return false
            return desc.substring(1, desc.length - 1).substringAfterLast('/') in nonNullSimpleClassNames
        }

    private val MethodNode.isConstructor get() = name == "<init>"


    private val hasValidationAnnotationsCache = concurrentMapOf<String, Boolean>()

    private fun ProcessContext.hasValidationAnnotations(classNode: ClassNode): Boolean {
        hasValidationAnnotationsCache[classNode.name]?.let { return it }

        val doesClassHaveValidationAnnotations = hasValidationAnnotationsCache.computeIfAbsent(classNode.name) compute@{ _ ->
            classNode.visibleAnnotations
                ?.any { it.desc == "Lorg/springframework/validation/annotation/Validated;" }
                ?.let { if (it) return@compute true }

            classNode.fields.default().asSequence()
                .filterNot(FieldNode::isStatic)
                .flatMap { it.visibleAnnotations.default().asSequence() + it.visibleTypeAnnotations.default().asSequence() }
                .any { isValidationAnnotation(it) }
                .let { if (it) return@compute true }

            classNode.methods.default().asSequence()
                .filterNot(MethodNode::isStatic)
                .filter { it.isGetter }
                .flatMap { it.visibleAnnotations.default().asSequence() + it.visibleTypeAnnotations.default().asSequence() }
                .any { isValidationAnnotation(it) }
                .let { if (it) return@compute true }

            return@compute false
        }
        if (doesClassHaveValidationAnnotations) {
            return true
        }

        val doParentClassesHaveValidationAnnotations = (sequenceOf(classNode.superName) + classNode.interfaces.default().asSequence())
            .filterNot { it.startsWith("java/") }
            .distinct()
            .any { parentClassInternalName ->
                val parentClassNode = parseBytecode(parentClassInternalName, SKIP_CODE or SKIP_DEBUG)
                    ?: return@any false

                return@any hasValidationAnnotations(parentClassNode)
            }
        if (doParentClassesHaveValidationAnnotations) {
            hasValidationAnnotationsCache[classNode.name] = true
            return true
        }

        val outerClassInternalName = classNode.outerClass
            ?: classNode.innerClasses.default().asSequence()
                .filter { it.name == classNode.name }
                .map(InnerClassNode::outerName)
                .filterNot { it.startsWith("java/") }
                .firstOrNull()
        if (outerClassInternalName != null
            && classNode.name.substringAfterLast('/').matches(builderClassNameRegEx)
        ) {
            val outerClassNode = parseBytecode(outerClassInternalName, SKIP_CODE or SKIP_DEBUG)
                ?: return false

            val outerClassHasValidationAnnotations = hasValidationAnnotations(outerClassNode)
            if (outerClassHasValidationAnnotations) {
                hasValidationAnnotationsCache[classNode.name] = true
                return true
            }
        }

        hasValidationAnnotationsCache[classNode.name] = false
        return false
    }

    private val MethodNode.isGetter: Boolean get() = name.matches(getterMethodNameRegEx)

    private val isValidationAnnotationCache = concurrentMapOf<String, Boolean>()

    private fun ProcessContext.isValidationAnnotation(annotation: AnnotationNode) = isValidationAnnotation(descriptorToClassInternalName(annotation))

    @Suppress("kotlin:S3776")
    private fun ProcessContext.isValidationAnnotation(classInternalName: ClassInternalName): Boolean {
        if (classInternalName.startsWith("java/")) return false

        if (classInternalName == "javax/validation/Valid") return true

        isValidationAnnotationCache[classInternalName]?.let { return it }

        val isValidationAnnotation = isValidationAnnotationCache.computeIfAbsent(classInternalName) compute@{ _ ->
            val annotationClassNode = parseBytecode(classInternalName, SKIP_CODE or SKIP_DEBUG)
                ?.takeIf(ClassNode::isAnnotation)
                ?: return@compute false

            return@compute annotationClassNode.visibleAnnotations
                ?.any { it.desc == "Ljavax/validation/Constraint;" }
                ?: false
        }
        if (isValidationAnnotation) {
            return true
        }

        val isRepeatableValidationAnnotation = run compute@{
            val repeatableAnnotationClassNode = parseBytecode(classInternalName, SKIP_CODE or SKIP_DEBUG)
                ?.takeIf(ClassNode::isAnnotation)
                ?: return@compute false

            val valueMethodReturnType = repeatableAnnotationClassNode.methods
                ?.firstOrNull { !it.isStatic && it.name == "value" && !it.hasParameters }
                ?.let { getReturnType(it.desc) }
                ?: return@compute false

            if (valueMethodReturnType.sort != Type.ARRAY) return@compute false
            if (valueMethodReturnType.dimensions != 1) return@compute false

            val typeToCheck = valueMethodReturnType.elementType
            if (typeToCheck.isPrimitive) return@compute false

            val classToCheckInternalName = typeToCheck.internalName
            if (classToCheckInternalName == "java/lang/Class") return@compute false
            if (classToCheckInternalName == "java/lang/String") return@compute false

            val annotationClassNode = parseBytecode(classToCheckInternalName, SKIP_CODE or SKIP_DEBUG)
                ?.takeIf(ClassNode::isAnnotation)
                ?: return@compute false

            val repeatableAnnotationNode = annotationClassNode.visibleAnnotations
                ?.firstOrNull { it.desc == "Ljava/lang/annotation/Repeatable;" }
                ?: return@compute false
            val containerType = repeatableAnnotationNode[JavaRepeatable::value]
                ?: return@compute false
            if (containerType.internalName != classInternalName) return@compute false

            val result = annotationClassNode.visibleAnnotations
                ?.any { it.desc == "Ljavax/validation/Constraint;" }
                ?: false
            isValidationAnnotationCache[classToCheckInternalName] = result
            return@compute result
        }
        if (isRepeatableValidationAnnotation) {
            isValidationAnnotationCache[classInternalName] = true
            return true
        }

        isValidationAnnotationCache[classInternalName] = false
        return false
    }


    private fun ProcessContext.parseBytecode(classInternalName: String, parsingOptions: Int = 0): ClassNode? {
        val bytecode = readBinaryResource("$classInternalName.class")
            ?: readClasspathBinaryResource("$classInternalName.class")
            ?: return null

        val classNode = ClassNode()
        ClassReader(bytecode).accept(classNode, parsingOptions)
        return classNode
    }


    private fun descriptorToClassInternalName(descriptor: String): ClassInternalName {
        return getType(descriptor).internalName
    }

    private fun descriptorToClassInternalName(annotationNode: AnnotationNode) = descriptorToClassInternalName(annotationNode.desc)


    override fun getStage() = POST_PROCESSING_STAGE

}


@AutoService
class InsertNullChecksProcessorFactory : ClassesProcessorsGradleTaskFactory {
    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.project.isPluginAppliedAndNotDisabled(InsertNullChecksPlugin::class.java)) return emptyList()
        return listOf(InsertNullChecksProcessor())
    }
}
