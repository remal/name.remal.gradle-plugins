package name.remal.gradle_plugins.plugins.dependencies

import name.remal.Ordered
import org.gradle.api.artifacts.Dependency

interface DependencyModifier : Ordered<DependencyModifier> {
    fun modify(dependency: Dependency)
}
