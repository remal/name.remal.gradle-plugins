package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import org.gradle.api.artifacts.Configuration.State.UNRESOLVED
import org.gradle.api.artifacts.ConfigurationContainer

@Deprecated("This plugin doesn't work as expected (all configurations extend allResolvable)")
@Plugin(
    id = "name.remal.all-resolvable-configuration",
    description = "Plugin that creates 'allResolvable' configuration and makes all resolvable configurations to extend it.",
    tags = ["configurations", "dependencies"]
)
class AllResolvableConfigurationsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun ConfigurationContainer.`Create 'allResolvable' configuration`() {
        val allResolvable = create("allResolvable") {
            it.description = "All resolvable configurations extend it"
        }

        all { conf ->
            if (conf == allResolvable) return@all
            if (conf.state != UNRESOLVED) return@all

            if (conf.isCanBeResolved) {
                conf.extendsFrom(allResolvable)
            }
        }
    }

}
