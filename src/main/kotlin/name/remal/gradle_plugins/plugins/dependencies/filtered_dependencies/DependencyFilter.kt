package name.remal.gradle_plugins.plugins.dependencies.filtered_dependencies

class DependencyFilter {

    val includes: MutableSet<String> = mutableSetOf()

    fun include(vararg patterns: String) {
        includes.addAll(patterns)
    }

    fun include(patterns: Iterable<String>) {
        includes.addAll(patterns)
    }


    val excludes: MutableSet<String> = mutableSetOf()

    fun exclude(vararg patterns: String) {
        excludes.addAll(patterns)
    }

    fun exclude(patterns: Iterable<String>) {
        excludes.addAll(patterns)
    }


    override fun hashCode(): Int {
        return includes.hashCode() * 31 + excludes.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is DependencyFilter) return false
        return includes == other.includes && excludes == other.excludes
    }

    override fun toString(): String {
        return "${DependencyFilter::class.java.simpleName}(includes=$includes, excludes=$excludes)"
    }

}
