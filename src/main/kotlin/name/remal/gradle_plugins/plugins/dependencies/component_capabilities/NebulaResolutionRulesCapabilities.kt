package name.remal.gradle_plugins.plugins.dependencies.component_capabilities

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.AbstractComponentCapabilities
import name.remal.gradle_plugins.plugins.dependencies.NebulaResolutionRules
import org.gradle.api.Project
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.capabilities.MutableCapabilitiesMetadata
import javax.inject.Inject

@AutoService
class NebulaResolutionRulesCapabilities @Inject constructor(project: Project) : AbstractComponentCapabilities(project) {

    override fun ModuleVersionIdentifier.process(capabilities: MutableCapabilitiesMetadata) {
        NebulaResolutionRules.addComponentCapabilities(this, capabilities)
    }

}
