package name.remal.gradle_plugins.plugins.environment_variables.infos

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.DefaultEnvironmentVariableInfo
import name.remal.gradle_plugins.dsl.EnvironmentVariableInfo
import name.remal.gradle_plugins.dsl.EnvironmentVariableInfoFactory
import name.remal.gradle_plugins.dsl.extensions.toHasEntries
import org.gradle.api.Project

@AutoService
class AWSEnvironmentVariableInfoFactory : EnvironmentVariableInfoFactory {

    override fun create(project: Project): List<EnvironmentVariableInfo> {
        if (project.buildscript.configurations.toHasEntries().containsClass("com.amazonaws.SDKGlobalConfiguration")) {
            return listOf(
                DefaultEnvironmentVariableInfo("AWS_ACCESS_KEY_ID", "Environment variable name for the AWS access key ID"),
                DefaultEnvironmentVariableInfo("AWS_ACCESS_KEY", "Alternate environment variable name for the AWS access key ID"),
                DefaultEnvironmentVariableInfo("AWS_SECRET_KEY", "Environment variable name for the AWS secret key"),
                DefaultEnvironmentVariableInfo("AWS_SECRET_ACCESS_KEY", "Alternate environment variable name for the AWS secret key"),
                DefaultEnvironmentVariableInfo("AWS_SESSION_TOKEN", "Environment variable name for the AWS session token"),
                DefaultEnvironmentVariableInfo("AWS_REGION", "Environment variable containing region used to configure clients"),
                DefaultEnvironmentVariableInfo("AWS_CONFIG_FILE", "Environment variable to set an alternate path to the shared config file (default path is ~/.aws/config)"),
                DefaultEnvironmentVariableInfo("AWS_CBOR_DISABLE", "Environment variable to disable CBOR protocol. This forces the request to be sent over the wire as a AWS JSON."),
                DefaultEnvironmentVariableInfo("AWS_ION_BINARY_DISABLE", "Environment variable to disable Ion binary protocol. This forces the request to be sent over the wire as Ion text."),
                DefaultEnvironmentVariableInfo("AWS_EC2_METADATA_DISABLED", "Environment variable to disable loading credentials or regions from EC2 Metadata instance service")
            )
                .map { it.withScope("Amazon Web Services (AWS)") }
        }

        return emptyList()
    }

}
