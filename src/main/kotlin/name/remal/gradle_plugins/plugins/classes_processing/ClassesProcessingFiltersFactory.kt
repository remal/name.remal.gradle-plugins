package name.remal.gradle_plugins.plugins.classes_processing

import org.gradle.api.Task

interface ClassesProcessingFiltersFactory {
    fun createClassesProcessingFilters(task: Task): List<ClassesProcessingFilter>
}
