package name.remal.gradle_plugins.plugins.code_quality.jacoco

import name.remal.gradle_plugins.dsl.PluginId

object JacocoPluginId : PluginId("jacoco")
