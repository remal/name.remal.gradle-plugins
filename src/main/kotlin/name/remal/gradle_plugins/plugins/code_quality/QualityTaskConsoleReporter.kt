package name.remal.gradle_plugins.plugins.code_quality

import org.gradle.api.Task

interface QualityTaskConsoleReporter<TaskType : Task> : QualityTaskReporter<TaskType> {

    fun printReportOf(task: TaskType)

}
