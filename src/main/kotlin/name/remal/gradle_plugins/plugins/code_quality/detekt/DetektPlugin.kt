package name.remal.gradle_plugins.plugins.code_quality.detekt

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.DoNotGenerateSimpleTest
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import org.gradle.api.GradleException

@Deprecated("use io.gitlab.arturbosch.detekt plugin instead")
@Plugin(
    id = "name.remal.detekt",
    description = "Plugin that executes Detekt checks for Kotlin sources.",
    tags = ["kotlin", "detekt"]
)
@DoNotGenerateSimpleTest
class DetektPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun fail() {
        throw GradleException("use 'io.gitlab.arturbosch.detekt' Gradle plugin instead of 'name.remal.detekt'")
    }

}
