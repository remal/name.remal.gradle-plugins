package name.remal.gradle_plugins.plugins.publish.bintray

import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.convention
import name.remal.gradle_plugins.dsl.extensions.get
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import java.net.URI

class BintrayMavenArtifactRepositoryDefault(private val mavenRepo: MavenArtifactRepository) : BintrayMavenArtifactRepository, MavenArtifactRepository by mavenRepo {

    init {
        mavenRepo.convention.addPlugin(BintrayMavenArtifactRepositoryExtension(mavenRepo))
    }

    override var apiUrl: String
        get() = mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].apiUrl
        set(value) {
            mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].apiUrl = value
        }

    override var owner: String?
        get() = mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].owner
        set(value) {
            mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].owner = value
        }

    override var repositoryName: String?
        get() = mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].repositoryName
        set(value) {
            mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].repositoryName = value
        }

    override var packageName: String?
        get() = mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].packageName
        set(value) {
            mavenRepo.convention[BintrayMavenArtifactRepositoryExtension::class.java].packageName = value
        }


    override fun setUrl(url: Any?) {
        throw UnsupportedOperationException(buildString {
            append("Use ")
            arrayOf(
                BintrayMavenArtifactRepositoryDefault::owner.setter,
                BintrayMavenArtifactRepositoryDefault::repositoryName.setter,
                BintrayMavenArtifactRepositoryDefault::packageName.setter
            ).joinToString(separator = " + ", transform = { "${it.name}()" })
            append(" instead")
        })
    }

    override fun setUrl(url: URI?) = setUrl(null as Any?)

}
