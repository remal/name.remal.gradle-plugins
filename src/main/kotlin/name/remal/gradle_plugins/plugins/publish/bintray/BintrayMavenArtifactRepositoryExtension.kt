package name.remal.gradle_plugins.plugins.publish.bintray

import name.remal.encodeURIComponent
import org.gradle.api.artifacts.repositories.MavenArtifactRepository

class BintrayMavenArtifactRepositoryExtension(private val mavenRepo: MavenArtifactRepository) {

    var apiUrl: String = "https://api.bintray.com/"

    var owner: String? = null
        set(value) {
            field = value
            updateUrl()
        }

    var repositoryName: String? = null
        set(value) {
            field = value
            updateUrl()
        }

    var packageName: String? = null
        set(value) {
            field = value
            updateUrl()
        }

    private fun updateUrl() {
        val apiUrl = this.apiUrl
        val owner = this.owner
        val repositoryName = this.repositoryName
        val packageName = this.packageName
        if (owner != null && repositoryName != null && packageName != null) {
            mavenRepo.setUrl(buildString {
                append(apiUrl.trimEnd('/'))
                append("/maven")
                append('/').append(encodeURIComponent(owner))
                append('/').append(encodeURIComponent(repositoryName))
                append('/').append(encodeURIComponent(packageName))
                append("/;publish=1;override=1")
            })
        }
    }

}
