package name.remal.gradle_plugins.plugins.publish.nexus_staging

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface NexusStagingApi {

    @GET("service/local/staging/profiles")
    fun getProfiles(): NexusResponseData<List<NexusStagingProfile>>

    @GET("service/local/staging/profile_repositories/{profileId}")
    fun getProfileRepositories(@Path("profileId") profileId: String): NexusResponseData<List<NexusStagingRepository>>

    fun getProfileRepositories(profile: NexusStagingProfile) = getProfileRepositories(profile.id)

    @GET("service/local/staging/repository/{repositoryId}")
    fun getRepository(@Path("repositoryId") repositoryId: String): NexusStagingRepository

    @GET("service/local/staging/repository/{repositoryId}/activity")
    fun getRepositoryActivity(@Path("repositoryId") repositoryId: String): List<NexusStagingRepositoryAction>

    @POST("service/local/staging/bulk/{action}")
    fun bulkAction(@Path("action") action: String, @Body data: NexusResponseData<NexusStagingBulkActionRequest>): Call<Void>

    fun bulkAction(action: String, vararg stagedRepositoryIds: String) = bulkAction(
        action,
        NexusResponseData(NexusStagingBulkActionRequest(stagedRepositoryIds = stagedRepositoryIds.toList()))
    )


    @GET("service/local/repositories/{repositoryId}/content/{relativePath}")
    fun getRepositoryContent(
        @Path("repositoryId") repositoryId: String,
        @Path("relativePath", encoded = true) relativePath: String
    ): NexusResponseData<List<NexusRepositoryContentItem>>

    @GET("service/local/repositories/{repositoryId}/content/{relativePath}?describe=info")
    fun getRepositoryContentInfo(
        @Path("repositoryId") repositoryId: String,
        @Path("relativePath", encoded = true) relativePath: String
    ): NexusResponseData<NexusRepositoryContentInfo>

}
