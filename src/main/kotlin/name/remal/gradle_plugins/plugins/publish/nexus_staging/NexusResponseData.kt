package name.remal.gradle_plugins.plugins.publish.nexus_staging

data class NexusResponseData<R>(val data: R)
