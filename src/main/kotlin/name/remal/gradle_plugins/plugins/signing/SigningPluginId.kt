package name.remal.gradle_plugins.plugins.signing

import name.remal.gradle_plugins.dsl.PluginId

object SigningPluginId : PluginId("signing")
