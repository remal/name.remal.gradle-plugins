package name.remal.gradle_plugins.plugins.merge_resources

import name.remal.Ordered
import org.gradle.api.Task

interface ResourceMergerFactory : Ordered<ResourceMergerFactory> {

    fun createResourceMerger(task: Task): List<ResourceMerger>

}
