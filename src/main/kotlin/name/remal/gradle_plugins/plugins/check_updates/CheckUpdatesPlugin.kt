package name.remal.gradle_plugins.plugins.check_updates

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.setupTasksDependenciesAfterEvaluateAllProjectsOrNow
import org.gradle.api.plugins.HelpTasksPlugin.HELP_GROUP
import org.gradle.api.tasks.TaskContainer

const val CHECK_UPDATES_TASK_NAME = "checkUpdates"

@Deprecated(message = "Use automatic dependency updates software like Renovate, Dependabot, etc...")
@Plugin(
    id = "name.remal.check-updates",
    description = "Plugin that provides tasks for discovering dependency and Gradle updates",
    tags = ["versions", "dependency-updates", "gradle-updates"]
)
@Suppress("DEPRECATION")
@ApplyPluginClasses(
    CheckDependencyUpdatesPlugin::class,
    CheckGradleUpdatesPlugin::class
)
class CheckUpdatesPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun TaskContainer.`Create checkUpdates actions`() {
        create(CHECK_UPDATES_TASK_NAME) { task ->
            task.project.setupTasksDependenciesAfterEvaluateAllProjectsOrNow { project ->
                project.allprojects.asSequence()
                    .flatMap { it.tasks.asSequence() }
                    .filter { it is CheckDependencyUpdates || it is CheckGradleUpdates }
                    .forEach { task.dependsOn(it) }
            }

            task.group = HELP_GROUP
        }
    }

}
