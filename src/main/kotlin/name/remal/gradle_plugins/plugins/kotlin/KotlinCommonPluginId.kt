package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinCommonPluginId : PluginId("kotlin-common", "kotlin-platform-common", "org.jetbrains.kotlin.common", "org.jetbrains.kotlin.platform.common")
