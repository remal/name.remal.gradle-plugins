package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.PluginId

object KotlinJvmPluginId : PluginId("kotlin", "kotlin-platform-jvm", "org.jetbrains.kotlin.jvm", "org.jetbrains.kotlin.platform.jvm")
