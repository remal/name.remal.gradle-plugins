package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.PluginCondition
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import org.gradle.api.Project

interface SkipForBuildOnCIMixin {

    @PluginCondition
    fun Project.isBuildOnLocal(): Boolean {
        val ci = getOrNull(CIExtension::class.java) ?: return true
        return ci.isBuildOnLocal
    }

}
