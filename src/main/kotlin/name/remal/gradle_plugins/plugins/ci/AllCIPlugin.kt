package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin

@Plugin(
    id = "name.remal.all-ci",
    description = "Plugin that applies all CI remal plugins.",
    tags = ["ci"]
)
@ApplyPluginClasses(
    CommonCIPlugin::class,
    GitHubActionsCIPlugin::class,
    GitlabCIPlugin::class,
    TravisCIPlugin::class
)
class AllCIPlugin : BaseReflectiveProjectPlugin()
