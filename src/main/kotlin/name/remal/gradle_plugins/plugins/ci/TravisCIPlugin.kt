package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.PluginCondition
import name.remal.gradle_plugins.dsl.WithPluginClasses
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.environment_variables.EnvironmentVariablesPlugin
import name.remal.gradle_plugins.plugins.vcs.VcsOperationsExtension
import name.remal.gradle_plugins.plugins.vcs.VcsOperationsPlugin
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import java.lang.System.getenv

@Plugin(
    id = "name.remal.travis-ci",
    description = "Plugin that helps to integrate with Travis CI.",
    tags = ["travis-ci", "travis"]
)
@ApplyPluginClasses(EnvironmentVariablesPlugin::class)
class TravisCIPlugin : AbstractCIPlugin() {

    @PluginCondition
    fun Project.`Check if CI == 'true' && TRAVIS == 'true'`(): Boolean {
        return getenv("CI") == "true" && getenv("TRAVIS") == "true"
    }

    @PluginAction(order = -1)
    fun ExtensionContainer.`Set 'ci' extension values`() {
        this[CIExtension::class.java].run {
            isBuildOnCI = true
            pipelineId = null
            buildId = getenv("TRAVIS_BUILD_NUMBER").nullIfEmpty()
                ?: getenv("TRAVIS_BUILD_ID").nullIfEmpty()
            stageName = getenv("TRAVIS_BUILD_STAGE_NAME").nullIfEmpty()
            jobName = getenv("TRAVIS_JOB_NAME").nullIfEmpty()
                ?: getenv("TRAVIS_JOB_NUMBER").nullIfEmpty()
                    ?: getenv("TRAVIS_JOB_ID").nullIfEmpty()
        }
    }

    @PluginActionsGroup("Configure name.remal.vcs-operations plugin")
    @WithPluginClasses(VcsOperationsPlugin::class)
    inner class ConfigureVcsOperationsPlugin {

        @PluginAction("vcsOperations.overwriteCurrentBranch = getenv('TRAVIS_BRANCH')")
        fun ExtensionContainer.setOverwriteCurrentBranch() {
            if (!getenv("TRAVIS_TAG").isNullOrEmpty()) {
                return
            }

            sequenceOf(
                "TRAVIS_PULL_REQUEST_BRANCH",
                "TRAVIS_BRANCH"
            )
                .map(System::getenv)
                .filterNotNull()
                .filterNot(String::isNotEmpty)
                .firstOrNull()
                ?.let { branch ->
                    this[VcsOperationsExtension::class.java].overwriteCurrentBranch = branch
                }
        }
    }

}
