package name.remal.gradle_plugins.plugins.testing

import name.remal.OrderedHigher
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.extensions.hasDependency
import org.gradle.api.artifacts.Configuration
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.testng.TestNGOptions

object TestFrameworkConfigurerTestNG : TestFrameworkConfigurer {
    override fun configure(task: Test) {
        task.useTestNG {
            it as TestNGOptions
            it.parallel = it.parallel ?: "methods"
        }
    }
}


@AutoService(TestFrameworkConfigurerDetector::class)
class TestFrameworkConfigurerDetectorTestNGDependency : TestFrameworkConfigurerDetector, OrderedHigher<TestFrameworkConfigurerDetector> {

    override fun detect(classpath: FileCollection): TestFrameworkConfigurer? {
        if (classpath !is Configuration) return null
        if (classpath.hasDependency("org.testng", "testng")) return TestFrameworkConfigurerJUnit
        return null
    }

}

@AutoService
class TestFrameworkConfigurerDetectorTestNGClasspath : TestFrameworkConfigurerDetector {

    override fun detect(classpath: FileCollection): TestFrameworkConfigurer? {
        if (CachedArtifactsCollection(classpath).containsClass("org.testng.annotations.Test")) return TestFrameworkConfigurerTestNG
        return null
    }

}
