package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.test_source_sets.TestTaskNameExtension
import org.gradle.api.tasks.SourceSet

val SourceSet.testTaskName: String get() = TestTaskNameExtension.getTestTaskName(this)
