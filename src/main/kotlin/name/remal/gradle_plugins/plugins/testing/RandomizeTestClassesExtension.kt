package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.dsl.Extension
import org.gradle.api.Project
import java.lang.Math.abs
import java.lang.System.currentTimeMillis
import java.lang.System.identityHashCode

@Extension
class RandomizeTestClassesExtension(private val project: Project) {

    companion object {
        private val seedAddition = currentTimeMillis() / 1000
    }

    var enabled: Boolean = true

    var seed: Long = abs(seedAddition - identityHashCode(project.rootProject).toLong() - identityHashCode(project.rootProject.gradle).toLong())

}
