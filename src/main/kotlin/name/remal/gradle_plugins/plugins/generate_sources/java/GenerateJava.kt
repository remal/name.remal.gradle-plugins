package name.remal.gradle_plugins.plugins.generate_sources.java

import com.google.common.base.Defaults
import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.fromLowerCamelToLowerHyphen
import name.remal.fromLowerHyphenToUpperUnderscore
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.afterEvaluateOrNow
import name.remal.gradle_plugins.dsl.extensions.findAndUnwrapProperties
import name.remal.gradle_plugins.dsl.extensions.id
import name.remal.gradle_plugins.dsl.extensions.javaPackageName
import name.remal.gradle_plugins.dsl.extensions.toConfigureKotlinFunction
import name.remal.gradle_plugins.dsl.extensions.toStringSmart
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateClassTask
import name.remal.nullIf
import name.remal.uncheckedCast
import name.remal.unwrapPrimitive
import name.remal.use
import name.remal.wrapPrimitive
import org.gradle.api.tasks.CacheableTask
import java.io.InvalidObjectException
import java.io.ObjectInputValidation
import java.io.Serializable
import java.lang.Character.isJavaIdentifierPart
import java.lang.Character.isJavaIdentifierStart
import java.math.BigDecimal
import java.math.BigInteger
import java.nio.charset.Charset
import java.util.ArrayList
import java.util.LinkedHashMap
import java.util.LinkedHashSet
import java.util.TreeMap

@BuildTask
@CacheableTask
@Suppress("LargeClass")
class GenerateJava : BaseGenerateClassTask<GeneratingJavaClassWriter>() {

    override fun classFile(packageName: String, simpleName: String, action: GeneratingJavaClassWriter.() -> Unit) {
        val relativePath = buildString {
            if (packageName.isNotEmpty()) append(packageName.replace('.', '/')).append('/')
            append(simpleName).append(".java")
        }
        addGenerateAction(relativePath) { file ->
            GeneratingJavaClassWriter(packageName, simpleName, file, relativePath, this, file.writer(Charset.forName(charset))).use(action)
        }
    }


    @Suppress("ComplexMethod", "LongMethod")
    fun buildInfo(packageName: String, simpleName: String, action: (params: BuildInfoParams) -> Unit) {
        val params = BuildInfoParamsImpl().apply(action)
        objectsToCacheBy.add(params)

        classFile(packageName, simpleName) {
            writePackage()
            writeSuppressWarnings()
            writeBlock("${if (params.isPublic) "public " else ""}class $simpleName") {

                val info = LinkedHashMap<String, String>()
                if (params.writeProjectId) info["PROJECT_ID"] = project.id
                if (params.writeProjectVersion) info["PROJECT_VERSION"] = project.version.toString()

                val values = TreeMap<String, String?>()
                params.properties.forEach { property, propertyValue ->
                    if (propertyValue != null) {
                        values[property] = propertyValue.unwrapProviders()?.toStringSmart()
                    } else {
                        values.putAll(project.findAndUnwrapProperties(property))
                    }
                }
                values.forEach { property, value ->
                    if (value == null) return@forEach
                    val name = buildString {
                        property
                            .let(String::fromLowerCamelToLowerHyphen)
                            .let(String::fromLowerHyphenToUpperUnderscore)
                            .forEachIndexed { index, char ->
                                if (0 == index) {
                                    append(if (isJavaIdentifierStart(char)) char else '_')
                                } else {
                                    append(if (isJavaIdentifierPart(char)) char else '_')
                                }
                            }
                    }
                    info[name] = value
                }

                info.forEach { name, value ->
                    append('\n')
                    writeNotNullAnnotation()
                    append("public static final ")
                    append("String ").append(name).append(" = ").append("new String(\"").append(escapeJava(value)).append("\")")
                    append(";\n")
                }

                append('\n')

            }
        }
    }

    fun buildInfo(packageName: String, simpleName: String, @DelegatesTo(BuildInfoParams::class, strategy = DELEGATE_FIRST) action: Closure<*>) = buildInfo(
        packageName,
        simpleName,
        action.toConfigureKotlinFunction()
    )


    fun buildInfo(packageName: String, action: (writer: BuildInfoParams) -> Unit) {
        buildInfo(packageName, "BuildInfo", action)
    }

    fun buildInfo(packageName: String, @DelegatesTo(BuildInfoParams::class, strategy = DELEGATE_FIRST) action: Closure<*>) = buildInfo(packageName, action.toConfigureKotlinFunction())


    fun buildInfo(action: (writer: BuildInfoParams) -> Unit) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            buildInfo(project.javaPackageName, action)
        }
    }

    fun buildInfo(@DelegatesTo(BuildInfoParams::class, strategy = DELEGATE_FIRST) action: Closure<*>) = buildInfo(action.toConfigureKotlinFunction())


    private class BuildInfoParamsImpl : BuildInfoParams {

        override var isPublic: Boolean = false
        override var writeProjectId: Boolean = true
        override var writeProjectVersion: Boolean = true

        val properties = mutableMapOf<String, Any?>()

        override fun writeProperty(property: String, propertyValue: Any) = apply {
            properties[property] = propertyValue
        }

        override fun writeProperty(property: String) = apply {
            properties[property] = null
        }

        override fun writeProperties(propertiesPattern: String) = writeProperty(propertiesPattern)

    }


    @Suppress("ComplexMethod", "LongMethod")
    fun stringType(packageName: String, simpleName: String) {
        classFile(packageName, simpleName) {
            writePackage()
            writeImport(Comparable::class.java)
            writeImport(Serializable::class.java)
            writeImport(List::class.java)
            writeImport(ArrayList::class.java)
            writeImport(Set::class.java)
            writeImport(LinkedHashSet::class.java)
            writeImport(Collection::class.java)
            writeImport(Iterable::class.java)
            writeImport(ObjectInputValidation::class.java)
            writeImport(InvalidObjectException::class.java)
            "io.swagger.annotations.ApiModel".let { if (isClassInClasspath(it)) writeln("@$it(value = \"string\")\n") }
            writeImmutableAnnotation()
            writeSuppressWarnings()
            writeBlock("public final class $simpleName implements Comparable<$simpleName>, Serializable, ObjectInputValidation") {
                writeln()
                writeln("private static final long serialVersionUID = 1;")

                writeln()
                writeln("private static final $simpleName EMPTY = new $simpleName(\"\");")

                writeln()
                writeNotNullAnnotation()
                writeContractAnnotation("null->fail; !null->!null")
                "com.fasterxml.jackson.annotation.JsonCreator".let { if (isClassInClasspath(it)) writeln("@$it\n") }
                writeBlock("public static $simpleName of(${notNullAnnotation}String value)") {
                    writeBlock("if (value == null)") {
                        writeln("throw new NullPointerException(\"value == null\");")
                    }
                    writeBlock("if (value.isEmpty())") {
                        writeln("return EMPTY;")
                    }
                    writeln("return new $simpleName(value);")
                }

                writeln()
                writeNullableAnnotation()
                writeContractAnnotation("null->null; !null->!null")
                writeBlock("public static $simpleName ofNullable(${nullableAnnotation}String value)") {
                    writeln("return value != null ? of(value) : null;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static List<$simpleName> of(${notNullAnnotation}List<String> values)") {
                    writeln("List<$simpleName> result = new ArrayList<>();")
                    writeBlock("for (String value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static Set<$simpleName> of(${notNullAnnotation}Set<String> values)") {
                    writeln("Set<$simpleName> result = new LinkedHashSet<>();")
                    writeBlock("for (String value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static Collection<$simpleName> of(${notNullAnnotation}Collection<String> values)") {
                    writeln("List<$simpleName> result = new ArrayList<>();")
                    writeBlock("for (String value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static Iterable<$simpleName> of(${notNullAnnotation}Iterable<String> values)") {
                    writeln("List<$simpleName> result = new ArrayList<>();")
                    writeBlock("for (String value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeln("private final String value;")

                writeln()
                writeBlock("private $simpleName(${notNullAnnotation}String value)") {
                    writeln("this.value = value;")
                }

                writeln()
                writeln("// for deserialization")
                writeSuppressWarnings()
                writeBlock("private $simpleName()") {
                    writeln("this.value = null;")
                }

                writeln()
                writeNotNullAnnotation()
                writeOverrideAnnotation()
                "com.fasterxml.jackson.annotation.JsonValue".let { if (isClassInClasspath(it)) writeln("@$it\n") }
                writeBlock("public String toString()") {
                    writeln("return value;")
                }

                writeln()
                writeOverrideAnnotation()
                writeBlock("public boolean equals(${nullableAnnotation}Object obj)") {
                    writeBlock("if (obj == this)") { writeln("return true;") }
                    writeBlock("if (obj == null || !(obj instanceof $simpleName))") { writeln("return false;") }
                    writeln("$simpleName other = ($simpleName) obj;")
                    writeln("return value.equals(other.value);")
                }

                writeln()
                writeOverrideAnnotation()
                writeBlock("public int hashCode()") {
                    writeln("return value.hashCode();")
                }

                writeln()
                writeOverrideAnnotation()
                writeBlock("public int compareTo($notNullAnnotation$simpleName other)") {
                    writeln("return value.compareTo(other.value);")
                }

                writeln()
                writeOverrideAnnotation()
                writeSuppressWarnings()
                writeBlock("public void validateObject() throws InvalidObjectException") {
                    writeBlock("if (value == null)") {
                        writeln("throw new InvalidObjectException(\"value == null\");")
                    }
                }

                writeln()
                writeBlock("public boolean contains(${notNullAnnotation}CharSequence charSequence)") {
                    writeln("return value.contains(charSequence);")
                }

                writeln()
                writeBlock("public boolean startsWith(${notNullAnnotation}CharSequence charSequence)") {
                    writeln("return value.startsWith(charSequence.toString());")
                }

                writeln()
                writeBlock("public boolean endsWith(${notNullAnnotation}CharSequence charSequence)") {
                    writeln("return value.endsWith(charSequence.toString());")
                }
            }
        }
    }

    fun stringType(simpleName: String) {
        project.afterEvaluateOrNow { _ ->
            stringType(project.javaPackageName, simpleName)
        }
    }


    @Suppress("ComplexMethod", "LongMethod")
    private fun numberType(packageName: String, simpleName: String, info: NumberTypeInfo) {
        classFile(packageName, simpleName) {
            writePackage()
            writeImport(Comparable::class.java)
            writeImport(Serializable::class.java)
            if (info.isNotPrimitive) {
                writeImport(ObjectInputValidation::class.java)
                writeImport(InvalidObjectException::class.java)
            }
            writeImport(List::class.java)
            writeImport(ArrayList::class.java)
            writeImport(Set::class.java)
            writeImport(LinkedHashSet::class.java)
            writeImport(Collection::class.java)
            writeImport(Iterable::class.java)
            writeImport(BigInteger::class.java)
            writeImport(BigDecimal::class.java)
            writeImmutableAnnotation()
            "io.swagger.annotations.ApiModel".let {
                if (isClassInClasspath(it)) {
                    append('@').append(it).append("(\"")
                    if (info.isInteger) {
                        append("int")
                    } else {
                        append("double")
                    }
                    append("\")")
                }
            }
            writeSuppressWarnings()
            writeBlock("public final class $simpleName implements Comparable<$simpleName>, Serializable${if (info.isNotPrimitive) ", ObjectInputValidation" else ""}") {
                writeln()
                writeln("private static final long serialVersionUID = 1;")

                writeln()
                if (info.isPrimitive) {
                    writeln("private static final $simpleName ZERO = new $simpleName(0);")
                    writeln("private static final $simpleName ONE = new $simpleName(1);")
                } else {
                    writeln("private static final $simpleName ZERO = new $simpleName(${info.type.simpleName}.valueOf(0));")
                    writeln("private static final $simpleName ONE = new $simpleName(${info.type.simpleName}.valueOf(1));")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static $simpleName of(int value)") {
                    writeBlock("if (value == 0)") {
                        writeln("return ZERO;")
                    }
                    writeBlock("if (value == 1)") {
                        writeln("return ONE;")
                    }
                    if (info.isPrimitive) {
                        if (info.primitiveType == Int::class.java.unwrapPrimitive()) {
                            writeln("return new $simpleName(value);")
                        } else {
                            writeln("return new $simpleName((${info.canonicalTypeName}) value);")
                        }
                    } else {
                        writeln("return of(${info.type.simpleName}.valueOf(value));")
                    }
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static $simpleName of(long value)") {
                    writeBlock("if (value == 0)") {
                        writeln("return ZERO;")
                    }
                    writeBlock("if (value == 1)") {
                        writeln("return ONE;")
                    }
                    if (info.isPrimitive) {
                        if (info.primitiveType == Int::class.java.unwrapPrimitive()) {
                            writeln("return new $simpleName(Math.toIntExact(value));")
                        } else if (info.primitiveType == Long::class.java.unwrapPrimitive()) {
                            writeln("return new $simpleName(value);")
                        } else {
                            writeln("return new $simpleName((${info.canonicalTypeName}) value);")
                        }
                    } else {
                        writeln("return of(${info.type.simpleName}.valueOf(value));")
                    }
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static $simpleName of(double value)") {
                    if (info.isPrimitive && info.isInteger) {
                        writeln("return of((long) value);")
                    } else {
                        writeBlock("if (value == 0.0)") {
                            writeln("return ZERO;")
                        }
                        writeBlock("if (value == 1.0)") {
                            writeln("return ONE;")
                        }
                        if (info.isPrimitive) {
                            if (info.primitiveType == Double::class.java.unwrapPrimitive()) {
                                writeln("return new $simpleName(value);")
                            } else {
                                writeln("return new $simpleName((${info.canonicalTypeName}) value);")
                            }
                        } else {
                            if (info.type == BigInteger::class.java) {
                                writeln("return of(BigInteger.valueOf((long) value));")
                            } else {
                                writeln("return of(${info.type.simpleName}.valueOf(value));")
                            }
                        }
                    }
                }

                writeln()
                writeln("private static final BigInteger BIG_INTEGER_ZERO = BigInteger.valueOf(0);")
                writeln("private static final BigInteger BIG_INTEGER_ONE = BigInteger.valueOf(1);")

                writeln()
                writeNotNullAnnotation()
                writeContractAnnotation("null->fail; !null->!null")
                writeBlock("public static $simpleName of(${notNullAnnotation}BigInteger value)") {
                    writeBlock("if (value == null)") {
                        writeln("throw new NullPointerException(\"value == null\");")
                    }
                    writeBlock("if (value.equals(BIG_INTEGER_ZERO))") {
                        writeln("return ZERO;")
                    }
                    writeBlock("if (value.equals(BIG_INTEGER_ONE))") {
                        writeln("return ONE;")
                    }
                    if (info.isPrimitive) {
                        if (info.isInteger) {
                            writeln("return of(value.longValue());")
                        } else {
                            writeln("return of(value.doubleValue());")
                        }
                    } else {
                        when (info.type) {
                            BigInteger::class.java -> writeln("return new $simpleName(value);")
                            BigDecimal::class.java -> writeln("return of(new BigDecimal(value));")
                            else -> throw UnsupportedOperationException(info.type.toString())
                        }
                    }
                }

                writeln()
                writeNullableAnnotation()
                writeContractAnnotation("null->null; !null->!null")
                writeBlock("public static $simpleName ofNullable(${nullableAnnotation}BigInteger value)") {
                    writeln("return value != null ? of(value) : null;")
                }

                writeln()
                writeln("private static final BigDecimal BIG_DECIMAL_ZERO = BigDecimal.valueOf(0);")
                writeln("private static final BigDecimal BIG_DECIMAL_ONE = BigDecimal.valueOf(1);")

                writeln()
                writeNotNullAnnotation()
                writeContractAnnotation("null->fail; !null->!null")
                "com.fasterxml.jackson.annotation.JsonCreator".let { if (isClassInClasspath(it)) writeln("@$it\n") }
                writeBlock("public static $simpleName of(${notNullAnnotation}BigDecimal value)") {
                    writeBlock("if (value == null)") {
                        writeln("throw new NullPointerException(\"value == null\");")
                    }
                    writeln("value = value.stripTrailingZeros();")
                    writeBlock("if (value.equals(BIG_DECIMAL_ZERO))") {
                        writeln("return ZERO;")
                    }
                    writeBlock("if (value.equals(BIG_DECIMAL_ONE))") {
                        writeln("return ONE;")
                    }
                    if (info.isPrimitive) {
                        if (info.isInteger) {
                            writeln("return of(value.longValue());")
                        } else {
                            writeln("return of(value.doubleValue());")
                        }
                    } else {
                        when (info.type) {
                            BigInteger::class.java -> writeln("return of(value.toBigInteger());")
                            BigDecimal::class.java -> writeln("return new $simpleName(value);")
                            else -> throw UnsupportedOperationException(info.type.toString())
                        }
                    }
                }

                writeln()
                writeNullableAnnotation()
                writeContractAnnotation("null->null; !null->!null")
                writeBlock("public static $simpleName ofNullable(${nullableAnnotation}BigDecimal value)") {
                    writeln("return value != null ? of(value) : null;")
                }

                writeln()
                writeNotNullAnnotation()
                writeContractAnnotation("null->fail; !null->!null")
                writeBlock("public static $simpleName of(${notNullAnnotation}Number value)") {
                    writeBlock("if (value == null)") {
                        writeln("throw new NullPointerException(\"value == null\");")
                    }
                    writeBlock("if (value instanceof BigInteger)") {
                        writeln("return of((BigInteger) value);")
                    }
                    writeBlock("if (value instanceof BigDecimal)") {
                        writeln("return of((BigDecimal) value);")
                    }
                    writeBlock("if (value instanceof Double)") {
                        writeln("return of(value.doubleValue());")
                    }
                    writeBlock("if (value instanceof Float)") {
                        writeln("return of(value.doubleValue());")
                    }
                    writeBlock("if (value instanceof Long)") {
                        writeln("return of(value.longValue());")
                    }
                    writeBlock("if (value instanceof Integer)") {
                        writeln("return of(value.intValue());")
                    }
                    writeBlock("if (value instanceof Short)") {
                        writeln("return of(value.intValue());")
                    }
                    writeBlock("if (value instanceof Byte)") {
                        writeln("return of(value.intValue());")
                    }
                    writeln("return of(value.doubleValue());")
                }

                writeln()
                writeNullableAnnotation()
                writeContractAnnotation("null->null; !null->!null")
                writeBlock("public static $simpleName ofNullable(${nullableAnnotation}Number value)") {
                    writeln("return value != null ? of(value) : null;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static List<$simpleName> of(${notNullAnnotation}List<Number> values)") {
                    writeln("List<$simpleName> result = new ArrayList<>();")
                    writeBlock("for (Number value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static Set<$simpleName> of(${notNullAnnotation}Set<Number> values)") {
                    writeln("Set<$simpleName> result = new LinkedHashSet<>();")
                    writeBlock("for (Number value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static Collection<$simpleName> of(${notNullAnnotation}Collection<Number> values)") {
                    writeln("List<$simpleName> result = new ArrayList<>();")
                    writeBlock("for (Number value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeBlock("public static Iterable<$simpleName> of(${notNullAnnotation}Iterable<Number> values)") {
                    writeln("List<$simpleName> result = new ArrayList<>();")
                    writeBlock("for (Number value : values)") {
                        writeBlock("if (value != null)") {
                            writeln("result.add(of(value));")
                        }
                    }
                    writeln("return result;")
                }

                writeln()
                writeNotNullAnnotation()
                writeContractAnnotation("null->fail; !null->!null")
                writeBlock("public static $simpleName of(${notNullAnnotation}String value)") {
                    writeBlock("if (value == null)") {
                        writeln("throw new NullPointerException(\"value == null\");")
                    }
                    writeBlock("if (\"0\".equals(value))") {
                        writeln("return ZERO;")
                    }
                    writeBlock("if (\"1\".equals(value))") {
                        writeln("return ONE;")
                    }
                    writeln("return of(new BigDecimal(value));")
                }

                writeln()
                writeNullableAnnotation()
                writeContractAnnotation("null->null; !null->!null")
                writeBlock("public static $simpleName ofNullable(${nullableAnnotation}String value)") {
                    writeln("return value != null ? of(value) : null;")
                }

                writeln()
                if (info.isNotPrimitive) writeNotNullAnnotation()
                writeln("private final ${info.canonicalTypeName} value;")

                writeln()
                writeBlock("private $simpleName(${if (info.isNotPrimitive) notNullAnnotation else ""}${info.canonicalTypeName} value)") {
                    writeln("this.value = value;")
                }

                writeln()
                writeln("// for deserialization")
                writeSuppressWarnings()
                writeBlock("private $simpleName()") {
                    writeln("this.value = ${escapeJava(info.defaultValue)};")
                }

                writeln()
                if (info.isNotPrimitive) writeNotNullAnnotation()
                "com.fasterxml.jackson.annotation.JsonValue".let { if (isClassInClasspath(it)) writeln("@$it\n") }
                writeBlock("public ${info.canonicalTypeName} getValue()") {
                    writeln("return value;")
                }

                writeln()
                writeNotNullAnnotation()
                writeOverrideAnnotation()
                writeBlock("public String toString()") {
                    if (info.isPrimitive) {
                        writeln("return String.valueOf(value);")
                    } else {
                        writeln("return value.toString();")
                    }
                }

                writeln()
                writeOverrideAnnotation()
                writeBlock("public boolean equals(${nullableAnnotation}Object obj)") {
                    writeBlock("if (obj == this)") { writeln("return true;") }
                    writeBlock("if (obj == null || !(obj instanceof $simpleName))") { writeln("return false;") }
                    writeln("$simpleName other = ($simpleName) obj;")
                    if (info.isPrimitive) {
                        writeln("return value == other.value;")
                    } else {
                        writeln("return value.equals(other.value);")
                    }
                }

                writeln()
                writeOverrideAnnotation()
                writeBlock("public int hashCode()") {
                    if (info.isPrimitive) {
                        writeln("return ${info.type.simpleName}.hashCode(value);")
                    } else {
                        writeln("return value.hashCode();")
                    }
                }

                writeln()
                writeOverrideAnnotation()
                writeBlock("public int compareTo($notNullAnnotation$simpleName other)") {
                    if (info.isPrimitive) {
                        writeln("return ${info.type.simpleName}.compare(value, other.value);")
                    } else {
                        writeln("return value.compareTo(other.value);")
                    }
                }

                if (info.isNotPrimitive) {
                    writeln()
                    writeOverrideAnnotation()
                    writeSuppressWarnings()
                    writeBlock("public void validateObject() throws InvalidObjectException") {
                        writeBlock("if (value == null)") {
                            writeln("throw new InvalidObjectException(\"value == null\");")
                        }
                    }
                }
            }
        }
    }

    private class NumberTypeInfo(
        type: Class<out Number>,
        isInteger: Boolean? = null
    ) {
        val type: Class<out Number> = type.wrapPrimitive().uncheckedCast()
        val primitiveType: Class<*>? = type.unwrapPrimitive().nullIf { !isPrimitive }
        val isInteger: Boolean = run {
            if (isInteger == true) return@run true
            if (Int::class.javaObjectType.isAssignableFrom(type)) return@run true
            if (Long::class.javaObjectType.isAssignableFrom(type)) return@run true
            if (BigInteger::class.java.isAssignableFrom(type)) return@run true
            if (Float::class.javaObjectType.isAssignableFrom(type)) return@run false
            if (Double::class.javaObjectType.isAssignableFrom(type)) return@run false
            if (BigDecimal::class.java.isAssignableFrom(type)) return@run false
            throw IllegalArgumentException("isInteger can't be defined from type $type")
        }
        val isPrimitive: Boolean = primitiveType != null
        val isNotPrimitive: Boolean get() = !isPrimitive
        val canonicalTypeName: String = primitiveType?.simpleName ?: type.simpleName
        val defaultValue: String = primitiveType?.let { Defaults.defaultValue(it) }?.toString() ?: "null"
    }

    fun integerType(packageName: String, simpleName: String) {
        numberType(packageName, simpleName, NumberTypeInfo(Int::class.java))
    }

    fun integerType(simpleName: String) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            integerType(project.javaPackageName, simpleName)
        }
    }

    fun longType(packageName: String, simpleName: String) {
        numberType(packageName, simpleName, NumberTypeInfo(Long::class.java))
    }

    fun longType(simpleName: String) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            longType(project.javaPackageName, simpleName)
        }
    }

    fun doubleType(packageName: String, simpleName: String) {
        numberType(packageName, simpleName, NumberTypeInfo(Double::class.java))
    }

    fun doubleType(simpleName: String) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            doubleType(project.javaPackageName, simpleName)
        }
    }

    fun bigIntegerType(packageName: String, simpleName: String) {
        numberType(packageName, simpleName, NumberTypeInfo(BigInteger::class.java))
    }

    fun bigIntegerType(simpleName: String) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            bigIntegerType(project.javaPackageName, simpleName)
        }
    }

    fun bigDecimalType(packageName: String, simpleName: String) {
        numberType(packageName, simpleName, NumberTypeInfo(BigDecimal::class.java))
    }

    fun bigDecimalType(simpleName: String) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            bigDecimalType(project.javaPackageName, simpleName)
        }
    }

}
