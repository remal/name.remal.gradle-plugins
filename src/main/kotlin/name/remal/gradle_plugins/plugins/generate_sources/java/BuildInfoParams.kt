package name.remal.gradle_plugins.plugins.generate_sources.java

import java.io.Serializable

interface BuildInfoParams : Serializable {

    var isPublic: Boolean

    fun isPublic(isPublic: Boolean) = apply {
        this.isPublic = isPublic
    }

    var writeProjectId: Boolean

    fun writeProjectId(writeProjectId: Boolean) = apply {
        this.writeProjectId = writeProjectId
    }

    var writeProjectVersion: Boolean

    fun writeProjectVersion(writeProjectVersion: Boolean) = apply {
        this.writeProjectVersion = writeProjectVersion
    }

    fun writeProperty(property: String, propertyValue: Any): BuildInfoParams
    fun writeProperty(property: String): BuildInfoParams
    fun writeProperties(propertiesPattern: String): BuildInfoParams

}
