package name.remal.gradle_plugins.plugins.generate_sources

import name.remal.default
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import java.io.File
import java.io.StringWriter

open class GeneratingStringWriter(protected val classpath: Iterable<File> = emptyList()) : StringWriter(), GeneratingWriterInterface {

    final override fun toString() = super<StringWriter>.toString()

    override fun write(cbuf: CharArray, from: Int, len: Int) = super<StringWriter>.write(cbuf, from, len)
    override fun write(obj: Any?) = super<StringWriter>.write(obj?.toString().default("null"))

    override fun append(csq: CharSequence?) = super<StringWriter>.append(csq)
    override fun append(csq: CharSequence?, start: Int, end: Int) = super<StringWriter>.append(csq, start, end)
    override fun append(c: Char) = super<StringWriter>.append(c)

    override fun isClassInClasspath(className: String) = CachedArtifactsCollection(classpath).containsClass(className)

}
