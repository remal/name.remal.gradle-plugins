package name.remal.gradle_plugins.api.classes_processing;

public interface ClassesProcessor extends Comparable<ClassesProcessor> {

    void process(
        byte[] bytecode,
        BytecodeModifier bytecodeModifier,
        String className,
        String resourceName,
        ProcessContext context
    );

    int PRIORITIZED_STAGE = -2_000_000;
    int DEFAULT_STAGE = 0;
    int POST_PROCESSING_STAGE = 900_000;
    int RELOCATION_STAGE = 1_900_000;
    int VALIDATION_STAGE = 2_000_000;
    int COLLECTION_STAGE = 2_100_000;

    default int getStage() {
        return DEFAULT_STAGE;
    }

    default int getOrder() {
        return 0;
    }

    @Override
    default int compareTo(ClassesProcessor other) {
        int result = Integer.compare(getStage(), other.getStage());
        if (0 == result) result = Integer.compare(getOrder(), other.getOrder());
        return result;
    }

}
