package name.remal.gradle_plugins.api.classes_processing;

import java.util.List;
import org.gradle.api.tasks.compile.AbstractCompile;

public interface ClassesProcessorsGradleTaskFactory {

    List<ClassesProcessor> createClassesProcessors(AbstractCompile compileTask);

}
